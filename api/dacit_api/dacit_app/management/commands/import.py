import os
import csv
import sys
import uuid
import hashlib
import logging
from core import settings

from tqdm import tqdm
from dacit_app.models import *
from datetime import datetime
from django.db import connection
from django.apps import apps
from django.utils import timezone
from django.core.management import BaseCommand, CommandError
from django.core.management.color import no_style
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from pathlib import Path

OBJ_MAPPING = {}

DATE_FORMAT = "%Y-%m-%d %H:%M:%S.%f"
TZINFO = "UTC"


def toDatetime(x):
    if x:
        if "." not in x:
            x += ".0"  # convert to proper date format

        return datetime.strptime(x, DATE_FORMAT).replace(tzinfo=TZINFO)

    return timezone.now()


class Create:
    def process(self):
        args = {"ignore_conflicts": True}

        if os.path.exists(self.file_path):
            with open(self.file_path, "r") as csv_file:
                data = csv.reader(csv_file, delimiter=";")
                processed_rows = []
                columns = next(data)

                for row in tqdm(data, f"Import {self.name}", file=sys.stdout):
                    obj = self.convert(dict(zip(columns, row)))

                    if obj is not None:
                        processed_rows.append(obj)

                    if len(processed_rows) > 5000:
                        try:
                            self.obj.objects.bulk_create(processed_rows, **args)
                        except Exception as error:
                            print(error)

                        processed_rows = []

                if processed_rows:
                    try:
                        self.obj.objects.bulk_create(processed_rows, **args)
                    except Exception as error:
                        print(error)
        else:
            sys.stdout(f"{self.file_path} does not exist.")


class ImportWordList(Create):
    name = "Audio"

    def __init__(self, folder_path):
        self.file_path = os.path.join(folder_path, "LINGUA_LIBRE_DATA_NEW.csv")
        self.obj = Audio

    def convert(self, row):
        # Create missing users
        gender = "M"
        if row.get("gender") == "female":
            gender = "F"
        elif row.get("gender") == "intersex":
            gender = "D"
        speaker, created = DacitUser.objects.get_or_create(
            wm_username=row.get("speaker"),
            defaults={
                "active_dialect": "ST",
                "active_language": "DE",
                "username": random.randint(100000000, 9999999999),
                "gender": gender,
            },
        )

        # Create missing Text_Stimulus
        ts, created = TextStimulus.objects.get_or_create(
            text=row.get("word"), defaults={"wd_lexeme_url": row.get("llid")}
        )

        # Finally create Audio

        return self.obj(
            text_stimulus=ts,
            speaker=speaker,
            audio=None,
            remote_url=row.get("file"),
            language="DE",
            dialect="ST",
        )


class ImportMinPair(Create):
    name = "Min_Pair"

    def __init__(self, folder_path, filename, min_pair_class):
        self.file_path = os.path.join(folder_path, filename)
        self.obj = MinPair
        self.min_pair_class = min_pair_class

    def convert(self, row):
        word_1 = None
        word_1_text = text = row.get("Wort_1")
        word_2 = None
        word_2_text = text = row.get("Wort_2")
        default_speaker, created = DacitUser.objects.get_or_create(
            active_dialect="ST",
            active_language="DE",
            username=1,
            public_id=1,
            gender="M",
        )

        word_1 = TextStimulus.objects.filter(text=word_1_text).first()
        if word_1 is None:
            word_1 = TextStimulus(
                text=word_1_text, creator=row.get("Madoo_Nutzername")
            )
            word_1.save()

        word_2 = TextStimulus.objects.filter(text=word_2_text).first()
        if word_2 is None:
            word_2 = TextStimulus(
                text=word_2_text, creator=row.get("Madoo_Nutzername")
            )
            word_2.save()

        if (word_1 is None) and (word_2 is None):
            print("Error, both words couldn't be found")

        filename_1 = row.get("Datei_T_Spalte1")
        if filename_1 is None:
            print("Missing filename in row: " + str(row))
        else:
            my_file = Path("media/audio/" + filename_1 + ".wav")
            if not my_file.is_file():
                print("file does not exist: " + str(my_file))

        filename_2 = row.get("Datei_T_Spalte2")
        if filename_2 is None:
            print("Missing filename in row: " + str(row))
        else:
            my_file = Path("media/audio/" + filename_2 + ".wav")
            if not my_file.is_file():
                print("file does not exist: " + str(my_file))

        if filename_1 is not None and filename_1 != "":
            audio_file = Audio(
                speaker=default_speaker,
                audio="audio/" + filename_1 + ".wav",
                language=default_speaker.active_language,
                dialect=default_speaker.active_dialect,
                text_stimulus=word_1,
            )
            audio_file.save()

        if filename_2 is not None and filename_2 != "":
            audio_file = Audio(
                speaker=default_speaker,
                audio="audio/" + filename_2 + ".wav",
                language=default_speaker.active_language,
                dialect=default_speaker.active_dialect,
                text_stimulus=word_2,
            )
            audio_file.save()

        return self.obj(
            min_pair_class=self.min_pair_class,
            first_part=word_1,
            second_part=word_2,
        )


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("--input", type=str, default="/import")

    def handle(self, *args, **options):
        start_time = timezone.now()

        if os.path.isdir(options["input"]):
            ImportWordList(options["input"]).process()
            ImportMinPair(options["input"], "B_W_MIN_PAARE.csv", "B_W").process()
            ImportMinPair(options["input"], "F_W_MIN_PAARE.csv", "F_W").process()
            ImportMinPair(options["input"], "H_R_MIN_PAARE.csv", "H_R").process()
            ImportMinPair(options["input"], "K_T_MIN_PAARE.csv", "K_T").process()
            ImportMinPair(options["input"], "PF_F_MIN_PAARE.csv", "PF_F").process()
            ImportMinPair(options["input"], "R_L_MIN_PAARE.csv", "R_L").process()
        else:
            raise CommandError("Input is not a directory.")

        end_time = timezone.now()
        duration = end_time - start_time

        txt = f"Import took {duration.total_seconds()} seconds."
        self.stdout.write(self.style.SUCCESS(txt))

        configs = [apps.get_app_config(x) for x in ["dacit_app"]]
        models = [list(config.get_models()) for config in configs]

        with connection.cursor() as cursor:
            for model in models:
                for sql in connection.ops.sequence_reset_sql(no_style(), model):
                    cursor.execute(sql)

        self.stdout.write(self.style.SUCCESS("Successfully reset AutoFields."))
