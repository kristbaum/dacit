# Generated by Django 4.1.7 on 2023-10-05 17:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dacit_app', '0009_alter_text_stimulus_text'),
    ]

    operations = [
        migrations.AddField(
            model_name='word_training_round',
            name='correct',
            field=models.BooleanField(null=True),
        ),
        migrations.AddField(
            model_name='word_training_round',
            name='user_ts_selection',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='dacit_app.text_stimulus'),
        ),
    ]
