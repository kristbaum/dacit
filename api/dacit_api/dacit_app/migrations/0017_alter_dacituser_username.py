# Generated by Django 4.2.6 on 2023-10-23 09:47

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dacit_app', '0016_remove_dacituser_email_alter_dacituser_username'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dacituser',
            name='username',
            field=models.CharField(max_length=255, unique=True, validators=[django.core.validators.RegexValidator(code='invalid_username', message='Username must be a number between 100000000000 and 9999999999999', regex='^[1-9][0-9]{11,13}$')]),
        ),
    ]
