# Generated by Django 4.2.6 on 2023-10-29 13:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dacit_app', '0018_alter_dacituser_username'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='text_stimulus',
            name='description',
        ),
        migrations.RemoveField(
            model_name='text_stimulus',
            name='wc_picture_url',
        ),
        migrations.RemoveField(
            model_name='text_stimulus',
            name='wd_item_url',
        ),
        migrations.AddField(
            model_name='audio',
            name='remote_url',
            field=models.URLField(max_length=1000, null=True),
        ),
        migrations.AddField(
            model_name='dacituser',
            name='wm_username',
            field=models.CharField(max_length=255, null=True, unique=True),
        ),
        migrations.AlterField(
            model_name='audio',
            name='audio',
            field=models.FileField(null=True, upload_to='audio'),
        ),
        migrations.AlterField(
            model_name='text_stimulus',
            name='creator',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
