import random
import logging
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.core.validators import MaxValueValidator, MinValueValidator, RegexValidator
from django.utils.translation import gettext_lazy as _


class Language(models.TextChoices):
    GERMAN = "DE", _("German")
    ENGLISH = "EN", _("English")


class Dialect(models.TextChoices):
    STANDARD = "ST", _("Standard")
    BAVARIAN = "BY", _("Bavarian")


class DacitUserManager(BaseUserManager):
    def create_user(self, username, password=None):
        """
        Creates and saves a User with the given username and password
        """
        if not username:
            raise ValueError("Users must have an username")

        logging.info("Creating user with username: " + str(username))

        user = self.model(username=username)

        # user.username = random.randint(100000000000, 9999999999999)
        user.public_id = int(user.username)
        logging.info("Setting this userid as publicid: " + str(user.public_id))

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password=None):
        """
        Creates and saves a superuser with the given email and password.
        """
        user = self.create_user(
            username,
            password=password,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class DacitUser(AbstractBaseUser):
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    username = models.CharField(
        max_length=255,
        validators=[
            RegexValidator(
                regex="^[1-9][0-9]{8}$",
                message="Username must be a number between 100000000 and 9999999999",
                code="invalid_username",
            )
        ],
        unique=True,
    )

    public_id = models.BigIntegerField(unique=True, null=True)

    voice_share_pin = random.randint(10000, 99999)
    wm_username = models.CharField(max_length=255, null=True, unique=True)

    objects = DacitUserManager()

    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = []

    def __str__(self):
        return str(self.username)

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        return self.is_admin

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        return self.is_admin

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        return self.is_admin

    active_language = models.CharField(
        max_length=2,
        choices=Language.choices,
        default=Language.GERMAN,
    )

    active_dialect = models.CharField(
        max_length=2,
        choices=Dialect.choices,
        default=Dialect.STANDARD,
    )

    class Manufacturer(models.TextChoices):
        COCHLEARLIMITED = "CL", _("Cochlear Limited")
        ADVANCEDBIONICS = "AB"
        _("Advanced Bionics")
        MEDEL = "ME", _("MED-EL")
        NEURELEC = "NC", _("Neurelec")
        NUROTRON = "NN", _("Nurotron")

    manufacturer = models.CharField(
        max_length=2, null=True, choices=Manufacturer.choices, default=None
    )

    class Gender(models.TextChoices):
        FEMALE = "F", _("Female")
        MALE = "M", _("Male")
        DIVERS = "D", _("Divers")

    gender = models.CharField(
        max_length=1, choices=Gender.choices, null=True, default=None
    )
    min_pair_difficulty = models.IntegerField(
        default=1, validators=[MinValueValidator(1), MaxValueValidator(100)]
    )
    speaker_dif_difficulty = models.IntegerField(
        default=1, validators=[MinValueValidator(1), MaxValueValidator(100)]
    )
    word_training_difficulty = models.IntegerField(
        default=1, validators=[MinValueValidator(1), MaxValueValidator(100)]
    )
    dynamic_difficulity = models.BooleanField(default=True)

    created = models.DateTimeField(auto_now_add=True)


class UserRelationship(models.Model):
    user = models.ForeignKey(
        DacitUser, on_delete=models.CASCADE, related_name="active_user"
    )
    target_user = models.ForeignKey(DacitUser, on_delete=models.CASCADE)
    self_description = models.CharField(max_length=500)
    unique_together = ["user", "target_user"]

    class RelationshipTypes(models.TextChoices):
        PARENT = "P", _("Parent")
        SIBLING = "S", _("Sibling")
        FRIEND = "F", _("Friend")
        GRANDPARENT = "G", _("Grandparent")
        THERAPIST = "T", _("Therapist")

    relationship = models.CharField(
        max_length=1,
        choices=RelationshipTypes.choices,
        default=RelationshipTypes.FRIEND,
    )


class TextStimulus(models.Model):
    text = models.CharField(max_length=500, blank=False, unique=True)
    # text = models.CharField(max_length=500, blank=False)
    user_audio_creatable = models.BooleanField(default=True)

    language = models.CharField(
        max_length=2, choices=Language.choices, default=Language.GERMAN, blank=False
    )

    wd_lexeme_url = models.CharField(max_length=50)
    creator = models.CharField(max_length=50, null=True)

    def __str__(self):
        return str(self.text)


class TextStimulusSent(models.Model):
    user = models.ForeignKey(DacitUser, on_delete=models.CASCADE)
    text_stimulus = models.ForeignKey(TextStimulus, on_delete=models.CASCADE)

    # Number of times the user has had this text stimulus
    sent = models.IntegerField(default=0)
    # skipped = models.IntegerField(default=0)
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = (
            "user",
            "text_stimulus",
        )

    def __str__(self):
        return f"{self.user.username} - {self.text_stimulus.text}"


class Audio(models.Model):
    text_stimulus = models.ForeignKey(TextStimulus, on_delete=models.CASCADE)
    speaker = models.ForeignKey(DacitUser, on_delete=models.CASCADE)
    audio = models.FileField(upload_to="audio", null=True)
    remote_url = models.URLField(max_length=1000, null=True)
    quality = models.IntegerField(
        default=50, validators=[MinValueValidator(1), MaxValueValidator(100)]
    )
    difficulty = models.IntegerField(
        default=50, validators=[MinValueValidator(1), MaxValueValidator(100)]
    )
    language = models.CharField(
        max_length=2,
        choices=Language.choices,
        default=Language.GERMAN,
    )

    dialect = models.CharField(
        max_length=2,
        choices=Dialect.choices,
        default=Dialect.STANDARD,
    )

    def __str__(self):
        return str(self.audio.name)

    def get_audio_ref(self):
        if self.remote_url:
            return self.remote_url
        return self.audio.name.rsplit("/", 1)[-1].rsplit(".", 1)[0]


class AudioToUsers(models.Model):
    user = models.ForeignKey(DacitUser, on_delete=models.CASCADE)
    audio = models.ForeignKey(Audio, on_delete=models.CASCADE)
    ts_sent = models.DateTimeField(auto_now_add=True)
    ts_recieved = models.DateTimeField()
    skipped = models.BooleanField()
    mode = models.CharField(max_length=100)


class MinPair(models.Model):
    first_part = models.ForeignKey(
        TextStimulus, on_delete=models.CASCADE, blank=False, related_name="first_part"
    )
    second_part = models.ForeignKey(
        TextStimulus, on_delete=models.CASCADE, blank=False, related_name="second_part"
    )

    class MinPairClass(models.TextChoices):
        B_W = "B_W"
        F_W = "F_W"
        H_R = "H_R"
        K_T = "K_T"
        PF_F = "PF_F"
        R_L = "R_L"

    min_pair_class = models.CharField(
        max_length=4,
        choices=MinPairClass.choices,
        default=None,
    )

    def __str__(self):
        return str(self.first_part) + " " + str(self.second_part)


class MinPairRound(models.Model):
    user = models.ForeignKey(DacitUser, on_delete=models.CASCADE)
    min_pair = models.ForeignKey(MinPair, on_delete=models.CASCADE)

    answer_equal = models.BooleanField(null=True)
    answer_correct = models.BooleanField(null=True)
    # 0: 1=1, 2=2 send both stimuli
    # 1: 1=1, 2=1 send the first twice
    # 2: 1=2, 2=2 send the second twice
    send_config = models.IntegerField(default=0)

    first_audio_repeated = models.IntegerField(default=0)
    second_audio_repeated = models.IntegerField(default=0)
    # skipped = models.IntegerField(default=0)
    timestamp_sent = models.DateTimeField(auto_now_add=True)
    timestamp_recieved = models.DateTimeField(null=True)

    def __str__(self):
        return f"{self.user.username} - {self.min_pair}"


class WordTrainingRound(models.Model):
    user = models.ForeignKey(DacitUser, on_delete=models.CASCADE)
    sent_audio = models.ForeignKey(Audio, on_delete=models.CASCADE)

    sent_stimuli = models.ManyToManyField(TextStimulus, related_name="sent_stimuli")

    answer_stimulus = models.ForeignKey(
        TextStimulus, on_delete=models.CASCADE, related_name="answer_stimulus"
    )

    timestamp_sent = models.DateTimeField(auto_now_add=True)

    # Results
    audio_repeated = models.IntegerField(default=0)
    timestamp_recieved = models.DateTimeField(null=True)
    user_ts_selection = models.ForeignKey(
        TextStimulus, on_delete=models.CASCADE, null=True
    )
    answer_correct = models.BooleanField(null=True)

    def __str__(self):
        return f"{self.user.username} - {self.sent_audio}"


class SpeakerDisRound(models.Model):
    user = models.ForeignKey(DacitUser, on_delete=models.CASCADE, related_name="user")

    first_audio = models.ForeignKey(
        Audio, on_delete=models.CASCADE, related_name="first_audio"
    )
    second_audio = models.ForeignKey(
        Audio, on_delete=models.CASCADE, related_name="second_audio"
    )
    first_audio_repeated = models.IntegerField(null=True)
    second_audio_repeated = models.IntegerField(null=True)
    # skipped = models.IntegerField(default=0)
    is_answer_equal = models.BooleanField(null=True)
    answer_correct = models.BooleanField(null=True)
    timestamp_sent = models.DateTimeField(auto_now_add=True)
    timestamp_recieved = models.DateTimeField(null=True)

    def __str__(self):
        return f"{self.user.username} - {self.pk}"
