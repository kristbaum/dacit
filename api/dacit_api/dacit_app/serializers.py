from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator
from dacit_app.models import TextStimulus, MinPair, DacitUser, UserRelationship


class TextStimulusSerializer(serializers.ModelSerializer):
    class Meta:
        model = TextStimulus
        # fields = ['text', 'id']
        fields = "__all__"


class MinPairSerializer(serializers.ModelSerializer):
    class Meta:
        model = MinPair
        fields = "__all__"


class DacitUserSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        user = DacitUser.objects.create_user(**validated_data)
        return user

    class Meta:
        model = DacitUser
        fields = (
            #            'name',
            "email",
            "password",
        )
        validators = [
            UniqueTogetherValidator(queryset=DacitUser.objects.all(), fields=["email"])
        ]


class MinPairResponseSerializer(serializers.Serializer):
    min_pair_round_id = serializers.IntegerField()
    answer_equal = serializers.BooleanField()
    first_audio_repeated = serializers.IntegerField()
    second_audio_repeated = serializers.IntegerField()


class WordTrainingResponseSerializer(serializers.Serializer):
    word_training_id = serializers.IntegerField()
    audio_repeated = serializers.IntegerField()
    user_ts_selection = serializers.IntegerField()


class SpeakerDisResponseSerializer(serializers.Serializer):
    speaker_dis_id = serializers.IntegerField()
    first_audio_repeated = serializers.IntegerField()
    second_audio_repeated = serializers.IntegerField()
    is_answer_equal = serializers.BooleanField()


class UserRelationshipResponseSerializer(serializers.Serializer):
    target_user = serializers.CharField()
    voice_share_pin = serializers.IntegerField()
