import logging
from random import choice
from django.utils import timezone
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from dacit_app.models import DacitUser, Audio, MinPair, MinPairRound
from dacit_app.serializers import MinPairResponseSerializer


class MinPairLogic(APIView):
    # return a random single min pair
    def get(self, request):
        # Initialize some values
        json_min_pair = {}
        first_audio = None
        first_stimulus = None
        second_audio = None
        second_stimulus = None
        category = None

        speaker = request.query_params.get("speaker")
        if speaker is None:
            selected_speaker = DacitUser.objects.filter(username=1).first()
        else:
            selected_speaker = DacitUser.objects.get(pk=speaker)

        # TODO: Filter if min_pair has been viewed yet
        text_category = request.query_params.get("category")
        for min_pair_cat in MinPair.MinPairClass.choices:
            if text_category == min_pair_cat[0]:
                category = min_pair_cat
                break

        if category is None:
            logging.warning("Category not found, returning random category")
            # TODO: choice doesn't work yet, only ('K_T', 'K T') examples exist!
            category = choice(MinPair.MinPairClass.choices)
            # category = ('K_T', 'K T')
            logging.info(category)
            # print(Min_Pair.Min_Pair_Class.choices)
            # return Response({"error": "Category not defined"}, status=status.HTTP_404_NOT_FOUND)

        class_selection = MinPair.objects.filter(min_pair_class=category[0])
        pks = class_selection.values_list("pk", flat=True)
        random_pk = choice(pks)
        min_pair = class_selection.get(pk=random_pk)
        json_min_pair["category"] = min_pair.min_pair_class
        logging.info(min_pair)
        # Decide to send equal or not
        send_config = choice([0, 1, 2])
        if send_config != 0:
            if send_config == 1:
                first_audio = Audio.objects.filter(
                    text_stimulus=min_pair.first_part, speaker=selected_speaker
                )[0]
                first_stimulus = min_pair.first_part
                second_audio = first_audio
                second_stimulus = first_stimulus
            else:
                second_audio = Audio.objects.filter(
                    text_stimulus=min_pair.second_part, speaker=selected_speaker
                )[0]
                second_stimulus = min_pair.second_part
                first_audio = second_audio
                first_stimulus = second_stimulus
        else:
            first_audio = Audio.objects.filter(
                text_stimulus=min_pair.first_part, speaker=selected_speaker
            )[0]
            first_stimulus = min_pair.first_part
            second_audio = Audio.objects.filter(
                text_stimulus=min_pair.second_part, speaker=selected_speaker
            )[0]
            second_stimulus = min_pair.second_part

        json_min_pair["first_stimulus"] = first_stimulus.text
        # remove path and extension from filename
        json_min_pair["first_audio"] = first_audio.audio.name.rsplit("/", 1)[-1].rsplit(
            ".", 1
        )[0]
        json_min_pair["second_stimulus"] = second_stimulus.text
        # remove path and extension from filename
        json_min_pair["second_audio"] = second_audio.audio.name.rsplit("/", 1)[
            -1
        ].rsplit(".", 1)[0]

        min_pair_round = MinPairRound.objects.create(
            user=request.user, min_pair=min_pair, send_config=send_config
        )
        min_pair_round.save()
        json_min_pair["min_pair_round_id"] = min_pair_round.pk
        logging.info(json_min_pair)
        return Response(json_min_pair)

    def post(self, request):
        logging.info("Recieve minpair result")
        logging.info(request.data)
        serializer = MinPairResponseSerializer(data=request.data)
        if serializer.is_valid(raise_exception=ValueError):
            try:
                min_pair_round = MinPairRound.objects.get(
                    pk=serializer.validated_data.get("min_pair_round_id")
                )
                logging.info(min_pair_round)
                min_pair_round.answer_equal = serializer.validated_data.get(
                    "answer_equal"
                )
                min_pair_round.first_audio_repeated = serializer.validated_data.get(
                    "first_audio_repeated"
                )
                min_pair_round.second_audio_repeated = serializer.validated_data.get(
                    "second_audio_repeated"
                )
                min_pair_round.timestamp_recieved = timezone.now()
                if min_pair_round.send_config == 0:
                    if min_pair_round.answer_equal:
                        min_pair_round.answer_correct = False
                    else:
                        min_pair_round.answer_correct = True
                else:
                    if min_pair_round.answer_equal:
                        min_pair_round.answer_correct = True
                    else:
                        min_pair_round.answer_correct = False
                min_pair_round.save()
                return Response(status=status.HTTP_200_OK)
            except Exception as e:
                logging.error(e)
                logging.error("No minpair found")
                return Response(status=status.HTTP_404_NOT_FOUND)

        return Response(
            {
                "error": True,
                "error_msg": serializer.error_messages,
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
