import logging
from random import choice
from django.utils import timezone
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from dacit_app.models import Audio, TextStimulus, SpeakerDisRound
from dacit_app.serializers import TextStimulusSerializer, SpeakerDisResponseSerializer
from django.db.models import Count


class SpeakerDisLogic(APIView):
    def get(self, request):
        json_speaker_dis = {}
        speaker_dis = None
        # Different modes:
        # 0. 2 recordings of the same stimulus, different speakers
        # 1. 2 recodings of different stimuli, different speakers

        possible_stimuli = TextStimulus.objects.annotate(
            number_of_audios=Count("audio")
        )  # annotate the queryset
        mode0_possible_stimuli = possible_stimuli.filter(
            number_of_audios__gte=2
        )  # filter the queryset

        if len(mode0_possible_stimuli) > 0:
            logging.info(
                "Number of stimuli with two audios or more: %s",
                len(mode0_possible_stimuli),
            )
            mode = 0
        else:
            logging.info("No stimuli with 2 audios or more")
            mode = 1

        if mode == 0:
            random_stimulus = choice(mode0_possible_stimuli)
            possible_audios = Audio.objects.filter(text_stimulus=random_stimulus)
            # Todo: make it a 50/50 chance
            random_first_audio = choice(possible_audios)
            random_second_audio = choice(possible_audios)

            json_speaker_dis["mode"] = mode
            json_speaker_dis["first_stimulus"] = TextStimulusSerializer(
                random_stimulus
            ).data
            json_speaker_dis["second_stimulus"] = json_speaker_dis["first_stimulus"]
            if random_first_audio.speaker == random_second_audio.speaker:
                json_speaker_dis["same_speaker"] = True
            else:
                json_speaker_dis["same_speaker"] = False
            json_speaker_dis["first_audio"] = random_first_audio.get_audio_ref()
            json_speaker_dis["second_audio"] = random_second_audio.get_audio_ref()
            speaker_dis = SpeakerDisRound.objects.create(
                user=request.user,
                first_audio=random_first_audio,
                second_audio=random_second_audio,
            )
            speaker_dis.save()
        else:
            random_stimulus1 = choice(possible_stimuli)
            random_stimulus2 = choice(possible_stimuli)
            while random_stimulus1 == random_stimulus2:
                random_stimulus2 = choice(possible_stimuli)

            possible_audios1 = Audio.objects.filter(text_stimulus=random_stimulus1)
            possible_audios2 = Audio.objects.filter(text_stimulus=random_stimulus2)

            random_first_audio = choice(possible_audios1)
            random_second_audio = choice(possible_audios2)

            json_speaker_dis["mode"] = mode
            json_speaker_dis["first_stimulus"] = TextStimulusSerializer(
                random_stimulus1
            ).data
            json_speaker_dis["second_stimulus"] = TextStimulusSerializer(
                random_stimulus2
            ).data
            if random_first_audio.speaker == random_second_audio.speaker:
                json_speaker_dis["same_speaker"] = True
            else:
                json_speaker_dis["same_speaker"] = False
            json_speaker_dis["first_audio"] = random_first_audio.get_audio_ref()
            json_speaker_dis["second_audio"] = random_second_audio.get_audio_ref()
            speaker_dis = SpeakerDisRound.objects.create(
                user=request.user,
                first_audio=random_first_audio,
                second_audio=random_second_audio,
            )
            speaker_dis.save()

        json_speaker_dis["speaker_dis_id"] = speaker_dis.pk
        logging.info(json_speaker_dis)
        return Response(json_speaker_dis)

    def post(self, request):
        logging.info("Recieve SpeakerDis result")
        logging.info(request.data)
        serializer = SpeakerDisResponseSerializer(data=request.data)
        if serializer.is_valid(raise_exception=ValueError):
            try:
                speaker_dis_round = SpeakerDisRound.objects.get(
                    pk=serializer.validated_data.get("speaker_dis_id")
                )
                speaker_dis_round.first_audio_repeated = serializer.validated_data.get(
                    "first_audio_repeated"
                )
                speaker_dis_round.second_audio_repeated = serializer.validated_data.get(
                    "second_audio_repeated"
                )
                speaker_dis_round.is_answer_equal = serializer.validated_data.get(
                    "is_answer_equal"
                )
                speaker_dis_round.timestamp_recieved = timezone.now()
                if (
                    speaker_dis_round.first_audio.speaker
                    == speaker_dis_round.second_audio.speaker
                    and speaker_dis_round.is_answer_equal
                ) or (
                    speaker_dis_round.first_audio.speaker
                    != speaker_dis_round.second_audio.speaker
                    and not speaker_dis_round.is_answer_equal
                ):
                    speaker_dis_round.answer_correct = True
                else:
                    speaker_dis_round.answer_correct = False
                speaker_dis_round.save()
                return Response(status=status.HTTP_200_OK)

            except Exception as e:
                logging.error(e)
                return Response(status=status.HTTP_404_NOT_FOUND)

        return Response(
            {
                "error": True,
                "error_msg": serializer.error_messages,
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
