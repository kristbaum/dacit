import logging
from rest_framework.views import APIView
from rest_framework.response import Response
from django.utils import timezone

from dacit_app.models import (
    Audio,
    MinPairRound,
    SpeakerDisRound,
    WordTrainingRound,
)


class StatsLogic(APIView):
    # return statistics about the user
    def get(self, request):
        dacit_user = request.user
        stats = {}

        is_home = request.query_params.get("is_home")

        # number of training rounds today
        stats["training_today"] = (
            MinPairRound.objects.filter(
                user=dacit_user, timestamp_recieved__date__gte=timezone.now().date()
            ).count()
            + WordTrainingRound.objects.filter(
                user=dacit_user, timestamp_recieved__date__gte=timezone.now().date()
            ).count()
            + SpeakerDisRound.objects.filter(
                user=dacit_user, timestamp_recieved__date__gte=timezone.now().date()
            ).count()
        )

        # number of recordings by the user
        stats["num_audio"] = Audio.objects.filter(speaker=dacit_user).count()

        if not is_home:
            # number of times minpair played
            stats["num_minpair"] = MinPairRound.objects.filter(user=dacit_user).count()
            stats["num_minpair_week"] = MinPairRound.objects.filter(
                user=dacit_user,
                timestamp_recieved__date__gte=timezone.now().date()
                - timezone.timedelta(days=6),
            ).count()
            # number of correct answers in minpair
            stats["num_correct_minpair"] = MinPairRound.objects.filter(
                answer_correct=True, user=dacit_user
            ).count()
            stats["num_correct_minpair_week"] = MinPairRound.objects.filter(
                answer_correct=True,
                user=dacit_user,
                timestamp_recieved__date__gte=timezone.now().date()
                - timezone.timedelta(days=6),
            ).count()
            # number of times wordtraining played
            stats["num_wordtraining"] = WordTrainingRound.objects.filter(
                user=dacit_user
            ).count()
            stats["num_wordtraining_week"] = WordTrainingRound.objects.filter(
                user=dacit_user,
                timestamp_recieved__date__gte=timezone.now().date()
                - timezone.timedelta(days=6),
            ).count()
            # number of correct answers in wordtraining
            stats["num_correct_wordtraining"] = WordTrainingRound.objects.filter(
                answer_correct=True, user=dacit_user
            ).count()
            stats["num_correct_wordtraining_week"] = WordTrainingRound.objects.filter(
                answer_correct=True,
                user=dacit_user,
                timestamp_recieved__gte=timezone.now().date()
                - timezone.timedelta(days=6),
            ).count()
            stats["num_speakerdis"] = SpeakerDisRound.objects.filter(
                user=dacit_user
            ).count()
            stats["num_speakerdis_week"] = SpeakerDisRound.objects.filter(
                user=dacit_user,
                timestamp_recieved__date__gte=timezone.now().date()
                - timezone.timedelta(days=6),
            ).count()
            stats["num_correct_speakerdis"] = SpeakerDisRound.objects.filter(
                answer_correct=True, user=dacit_user
            ).count()
            stats["num_correct_speakerdis_week"] = SpeakerDisRound.objects.filter(
                answer_correct=True,
                user=dacit_user,
                timestamp_recieved__date__gte=timezone.now().date()
                - timezone.timedelta(days=6),
            ).count()

            # # average number of training rounds per hour
            # stats["training_data_hour"] = {}
            # for i in range(0, 24):
            #     stats["training_data_hour"][i] = (
            #         MinPairRound.objects.filter(
            #             user=dacit_user, timestamp_recieved__hour=i
            #         ).count()
            #         + WordTrainingRound.objects.filter(
            #             user=dacit_user, timestamp_recieved__hour=i
            #         ).count()
            #         + SpeakerDisRound.objects.filter(
            #             user=dacit_user, timestamp_recieved__hour=i
            #         ).count()
            #     )

            # number of training rounds last week per day
            stats["training_min_pair_week"] = {}
            stats["training_word_training_week"] = {}
            stats["training_speaker_dis_week"] = {}
            for i in range(0, 7):
                stats["training_min_pair_week"][7 - i] = MinPairRound.objects.filter(
                    user=dacit_user,
                    timestamp_recieved__date=timezone.now().date()
                    - timezone.timedelta(days=i),
                ).count()
                stats["training_word_training_week"][
                    7 - i
                ] = WordTrainingRound.objects.filter(
                    user=dacit_user,
                    timestamp_recieved__date=timezone.now().date()
                    - timezone.timedelta(days=i),
                ).count()
                stats["training_speaker_dis_week"][
                    7 - i
                ] = SpeakerDisRound.objects.filter(
                    user=dacit_user,
                    timestamp_recieved__date=timezone.now().date()
                    - timezone.timedelta(days=i),
                ).count()

        logging.info(stats)
        return Response(stats)
