import logging
from random import choice

from rest_framework.views import APIView
from rest_framework import status

from dacit_app.models import TextStimulus, TextStimulusSent
from dacit_app.serializers import TextStimulusSerializer
from rest_framework.response import Response


class TextStimulusLogic(APIView):
    def get(self, request):
        all_text_stimuli = TextStimulus.objects.all()

        text_stimuli_sent_to_user = TextStimulusSent.objects.filter(user=request.user)
        text_stimuli_sent_to_user_ids = [
            text_stimulus_sent.text_stimulus.id
            for text_stimulus_sent in text_stimuli_sent_to_user
        ]

        text_stimuli_to_send = all_text_stimuli.exclude(
            id__in=text_stimuli_sent_to_user_ids
        )

        if text_stimuli_to_send.exists():
            text_stimulus = choice(text_stimuli_to_send)

            text_stimulus_sent = TextStimulusSent(
                user=request.user, text_stimulus=text_stimulus, sent=1
            )
            text_stimulus_sent.save()

            logging.info(text_stimulus)
            json_stimulus = TextStimulusSerializer(text_stimulus).data
            return Response(json_stimulus)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)
