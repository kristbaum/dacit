import logging
from random import choice, random
import requests
from Levenshtein import distance

from django.utils import timezone
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from dacit_app.models import Audio, TextStimulus, UserRelationship, WordTrainingRound
from dacit_app.serializers import TextStimulusSerializer, WordTrainingResponseSerializer


class WordTrainingLogic(APIView):
    def get(self, request):
        num_answers = 4
        # Initialize some values
        json_word_training = {}

        # TODO: Filter for randomish speaker here
        selected_audio = self.select_recording(request.user)
        selected_stimulus = selected_audio.text_stimulus

        word_training = WordTrainingRound.objects.create(
            user=request.user,
            sent_audio=selected_audio,
            answer_stimulus=selected_stimulus,
        )

        json_word_training["selected_stimulus"] = TextStimulusSerializer(
            selected_stimulus
        ).data

        json_word_training["additional_answers"] = []
        for additional_stimuli in self.select_similar_stimuli(
            selected_stimulus, request.user.word_training_difficulty
        ):
            stimulus = TextStimulus.objects.get(pk=additional_stimuli[0])
            word_training.sent_stimuli.add(stimulus)
            json_word_training["additional_answers"].append(
                TextStimulusSerializer(stimulus).data
            )

        json_word_training["selected_audio"] = selected_audio.get_audio_ref()

        word_training.save()

        json_word_training["word_training_id"] = word_training.pk
        # json_word_training["additional_answers"] = addi
        logging.info(json_word_training)
        return Response(json_word_training)

    def select_recording(self, user):
        relationships = UserRelationship.objects.filter(user=user)
        # TODO: check if this is correct
        audio = Audio.objects.filter(
            speaker__in=relationships.values_list("target_user", flat=True)
        )
        if audio.exists():
            if random() < 0.2:
                selected_audio = audio.order_by("?").first()
            else:
                selected_audio = Audio.objects.order_by("?").first()
        else:
            selected_audio = Audio.objects.order_by("?").first()
        return selected_audio

    def select_similar_stimuli(self, selected_stimulus, difficulty=1, sample_size=200):
        if difficulty < 25:
            num_similar = 1
        elif difficulty < 50:
            num_similar = 2
        elif difficulty < 75:
            num_similar = 3
        else:
            num_similar = 4

        text_stimuli = TextStimulus.objects.values_list("pk", "text").order_by("?")[
            :sample_size
        ]

        # Remove selected_stimulus from text_stimuli
        text_stimuli = [
            (pk, text) for pk, text in text_stimuli if text != selected_stimulus.text
        ]

        stimuli_distances = [
            (pk, text, distance(selected_stimulus.text, text))
            for pk, text in text_stimuli
        ]
        sorted_stimuli = sorted(stimuli_distances, key=lambda x: x[2])

        difficulty_range = int((101 - difficulty) / 10)

        similar_stimuli = sorted_stimuli[
            difficulty_range : difficulty_range + num_similar
        ]
        return similar_stimuli

    def post(self, request):
        logging.info("Recieve WordTraining result")
        logging.info(request.data)
        serializer = WordTrainingResponseSerializer(data=request.data)
        if serializer.is_valid(raise_exception=ValueError):
            try:
                word_training_round = WordTrainingRound.objects.get(
                    pk=serializer.validated_data.get("word_training_id")
                )
                word_training_round.audio_repeated = serializer.validated_data.get(
                    "audio_repeated"
                )
                word_training_round.user_ts_selection = TextStimulus.objects.get(
                    pk=serializer.validated_data.get("user_ts_selection")
                )
                word_training_round.timestamp_recieved = timezone.now()

                self.model_progression(
                    request.user, word_training_round, word_training_round.sent_audio
                )

                return Response(status=status.HTTP_200_OK)

            except Exception as e:
                logging.error(e)
                return Response(status=status.HTTP_404_NOT_FOUND)

        return Response(
            {
                "error": True,
                "error_msg": serializer.error_messages,
            },
            status=status.HTTP_400_BAD_REQUEST,
        )

    def model_progression(self, user, word_training_round, audio):
        # Modify audio difficulty and quality
        audio = word_training_round.sent_audio
        logging.info(
            "%s selected: %s",
            str(word_training_round.answer_stimulus),
            str(word_training_round.user_ts_selection),
        )
        if word_training_round.answer_stimulus == word_training_round.user_ts_selection:
            # User answered correctly
            word_training_round.answer_correct = True
            if user.word_training_difficulty < 100:
                # Increase word training difficulty because answered correctly
                user.word_training_difficulty += 1
            if user.word_training_difficulty < 25:
                # Beginner
                # Decrease difficulty because a beginner answered correctly
                audio.difficulty -= 2
            elif user.word_training_difficulty < 50:
                # Intermediate
                # Decrease difficulty because an intermediate answered correctly
                audio.difficulty -= 1
            elif user.word_training_difficulty < 75:
                # Advanced
                # Increase quality because an advanced answered correctly
                audio.quality += 1
            else:
                # Expert
                # Increase quality because an expert answered correctly
                audio.quality += 2
        else:
            # User answered incorrectly
            word_training_round.answer_correct = False
            if user.word_training_difficulty > 0:
                # Decrease word training difficulty because answered incorrectly
                user.word_training_difficulty -= 1
            if user.word_training_difficulty < 25:
                # Beginner
                # Increase difficulty because a beginner answered incorrectly
                audio.difficulty += 2
            elif user.word_training_difficulty < 50:
                # Intermediate
                # Increase difficulty because an intermediate answered incorrectly
                audio.difficulty += 1
            elif user.word_training_difficulty < 75:
                # Advanced
                # Decrease quality because an advanced answered incorrectly
                audio.quality -= 1
            else:
                # Expert
                # Decrease quality because an expert answered incorrectly
                audio.quality -= 2
        user.save()
        audio.save()
        word_training_round.save()
