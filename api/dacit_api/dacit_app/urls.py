from django.urls import path
from . import views

urlpatterns = [
    # path('upload/', views.MultipartView.handle_upload, name='upload'),
    path("upload/<str:filename>/", views.FileUploadView.as_view(), name="upload"),
    path("download/<str:filename>/", views.Download.as_view(), name="download"),
    # path('ts/', views.TextStimuli.as_view(), name='ts'),
    path("sts/", views.TextStimulusClient.as_view(), name="sts"),
    path("minpair/", views.MinPairClient.as_view(), name="minpair"),
    path("speakerdis/", views.SpeakerDisClient.as_view(), name="speakerdis"),
    path("trainword/", views.WordTrainingClient.as_view(), name="trainword"),
    path("stats/", views.StatsClient.as_view(), name="stats"),
    path("user/", views.DacitUserRecordView.as_view(), name="users"),
    path("user_relationship/", views.UserRelationshipClient.as_view(), name="user_relationship"),
]
