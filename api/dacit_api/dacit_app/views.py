import logging

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import AllowAny
from rest_framework import status
from dacit_app.single_views.speaker_dis_logic import SpeakerDisLogic
from dacit_app.single_views.word_training_logic import WordTrainingLogic
from dacit_app.single_views.min_pair_logic import MinPairLogic
from dacit_app.single_views.text_stimulus_logic import TextStimulusLogic
from dacit_app.single_views.stats_logic import StatsLogic


from dacit_app.models import (
    DacitUser,
    TextStimulus,
    Audio,
    UserRelationship,
)
from dacit_app.serializers import (
    TextStimulusSerializer,
    DacitUserSerializer,
    UserRelationshipResponseSerializer,
)
from django.http import HttpResponse


class DacitUserRecordView(APIView):
    """
    API View to create or get a list of all the registered
    users. GET request returns the registered users whereas
    a POST request allows to create a new user.
    """

    # permission_classes = [IsAdminUser]

    # def get(self, format=None):
    #     users = CustomUser.objects.all()
    #     serializer = DacitUserSerializer(users, many=True)
    #     return Response(serializer.data)
    permission_classes = [AllowAny]

    def post(self, request):
        logging.info("Create user")
        serializer = DacitUserSerializer(data=request.data)
        logging.info("Create user")
        if serializer.is_valid(raise_exception=ValueError):
            serializer.create(validated_data=request.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(
            {
                "error": True,
                "error_msg": serializer.error_messages,
            },
            status=status.HTTP_400_BAD_REQUEST,
        )


class FileUploadView(APIView):
    parser_classes = [MultiPartParser]

    def post(self, request, filename):
        # logging.info("Filename: " + filename)
        # logging.info("Requestdata: " + str(request.data))
        logging.info(request.FILES)
        file_obj = request.FILES["file"]
        dacit_user = request.user

        matching_ts = TextStimulus.objects.get(pk=int(filename))

        if matching_ts is not None:
            # logging.info(matching_ts)

            # logging.info(file_obj)
            new_audio = Audio(
                text_stimulus=matching_ts,
                speaker=dacit_user,
                audio=file_obj,
                language=dacit_user.active_language,
                dialect=dacit_user.active_dialect,
            )
            new_audio.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class Download(APIView):
    permission_classes = [AllowAny]

    def get(self, request, filename):
        try:
            with open("media/audio/" + filename + ".wav", "rb") as read_file:
                response = HttpResponse(
                    read_file,
                    headers={
                        "Content-Type": "audio/wav",
                        "Content-Disposition": 'attachment; filename="audio.wav"',
                    },
                )
                return response
        except Exception as e:
            logging.error(e)
            return Response(
                {"error": "File not found"}, status=status.HTTP_404_NOT_FOUND
            )


class TextStimuli(APIView):
    # return a number of textstimuli
    def get(self, request):
        all_stimuli = TextStimulus.objects.order_by("text")[:5]
        logging.info(all_stimuli)
        json_stimuli = TextStimulusSerializer(all_stimuli).data
        return Response(json_stimuli)


class UserRelationshipClient(APIView):
    # return a random single min pair
    def get(self, request):
        # Initialize some values
        json_user_rel = {"pin": request.user.voice_share_pin}
        json_user_rel["provides_for"] = []
        json_user_rel["receives_from"] = []
        receives_from = UserRelationship.objects.filter(user=request.user)
        for user_rel in receives_from:
            json_user_rel["receives_from"].append(user_rel.target_user.username)

        provides_for = UserRelationship.objects.filter(target_user=request.user)
        for user_rel in provides_for:
            json_user_rel["provides_for"].append(user_rel.user.username)
        logging.info(json_user_rel)
        return Response(json_user_rel)

    def post(self, request):
        logging.info("Set new relationship")
        logging.info(request.data)
        serializer = UserRelationshipResponseSerializer(data=request.data)
        if serializer.is_valid(raise_exception=ValueError):
            target_user = DacitUser.objects.get(
                username=serializer.validated_data["target_user"]
            )
            if (
                target_user.voice_share_pin
                == serializer.validated_data["voice_share_pin"]
            ):
                UserRelationship.objects.create(
                    user=request.user, target_user=target_user
                )
                return Response(status=status.HTTP_201_CREATED)
            return Response(
                {
                    "error": True,
                    "error_msg": "Pin does not match",
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        return Response(
            {
                "error": True,
                "error_msg": serializer.error_messages,
            },
            status=status.HTTP_400_BAD_REQUEST,
        )


class StatsClient(StatsLogic):
    pass


class TextStimulusClient(TextStimulusLogic):
    pass


class MinPairClient(MinPairLogic):
    pass


class WordTrainingClient(WordTrainingLogic):
    pass


class SpeakerDisClient(SpeakerDisLogic):
    pass
