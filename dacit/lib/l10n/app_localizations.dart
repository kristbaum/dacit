import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart' as intl;

import 'app_localizations_de.dart';
import 'app_localizations_en.dart';

// ignore_for_file: type=lint

/// Callers can lookup localized strings with an instance of AppLocalizations
/// returned by `AppLocalizations.of(context)`.
///
/// Applications need to include `AppLocalizations.delegate()` in their app's
/// `localizationDelegates` list, and the locales they support in the app's
/// `supportedLocales` list. For example:
///
/// ```dart
/// import 'l10n/app_localizations.dart';
///
/// return MaterialApp(
///   localizationsDelegates: AppLocalizations.localizationsDelegates,
///   supportedLocales: AppLocalizations.supportedLocales,
///   home: MyApplicationHome(),
/// );
/// ```
///
/// ## Update pubspec.yaml
///
/// Please make sure to update your pubspec.yaml to include the following
/// packages:
///
/// ```yaml
/// dependencies:
///   # Internationalization support.
///   flutter_localizations:
///     sdk: flutter
///   intl: any # Use the pinned version from flutter_localizations
///
///   # Rest of dependencies
/// ```
///
/// ## iOS Applications
///
/// iOS applications define key application metadata, including supported
/// locales, in an Info.plist file that is built into the application bundle.
/// To configure the locales supported by your app, you’ll need to edit this
/// file.
///
/// First, open your project’s ios/Runner.xcworkspace Xcode workspace file.
/// Then, in the Project Navigator, open the Info.plist file under the Runner
/// project’s Runner folder.
///
/// Next, select the Information Property List item, select Add Item from the
/// Editor menu, then select Localizations from the pop-up menu.
///
/// Select and expand the newly-created Localizations item then, for each
/// locale your application supports, add a new item and select the locale
/// you wish to add from the pop-up menu in the Value field. This list should
/// be consistent with the languages listed in the AppLocalizations.supportedLocales
/// property.
abstract class AppLocalizations {
  AppLocalizations(String locale) : localeName = intl.Intl.canonicalizedLocale(locale.toString());

  final String localeName;

  static AppLocalizations? of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  static const LocalizationsDelegate<AppLocalizations> delegate = _AppLocalizationsDelegate();

  /// A list of this localizations delegate along with the default localizations
  /// delegates.
  ///
  /// Returns a list of localizations delegates containing this delegate along with
  /// GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate,
  /// and GlobalWidgetsLocalizations.delegate.
  ///
  /// Additional delegates can be added by appending to this list in
  /// MaterialApp. This list does not have to be used at all if a custom list
  /// of delegates is preferred or required.
  static const List<LocalizationsDelegate<dynamic>> localizationsDelegates = <LocalizationsDelegate<dynamic>>[
    delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
  ];

  /// A list of this localizations delegate's supported locales.
  static const List<Locale> supportedLocales = <Locale>[
    Locale('de'),
    Locale('en')
  ];

  /// No description provided for @minimalPairs.
  ///
  /// In en, this message translates to:
  /// **'Train Minimal Pairs'**
  String get minimalPairs;

  /// No description provided for @recordAudio.
  ///
  /// In en, this message translates to:
  /// **'Record Audio'**
  String get recordAudio;

  /// No description provided for @recordAudioDesc.
  ///
  /// In en, this message translates to:
  /// **'Record new audio'**
  String get recordAudioDesc;

  /// No description provided for @speakerDisambiguation.
  ///
  /// In en, this message translates to:
  /// **'Train Speaker Disambiguation'**
  String get speakerDisambiguation;

  /// No description provided for @settings.
  ///
  /// In en, this message translates to:
  /// **'Settings'**
  String get settings;

  /// No description provided for @about.
  ///
  /// In en, this message translates to:
  /// **'About'**
  String get about;

  /// No description provided for @welcome.
  ///
  /// In en, this message translates to:
  /// **'Welcome to Dacit\na training app for people with cochlear implants'**
  String get welcome;

  /// No description provided for @whichdialect.
  ///
  /// In en, this message translates to:
  /// **'which dialect do you speak?'**
  String get whichdialect;

  /// No description provided for @whichyear.
  ///
  /// In en, this message translates to:
  /// **'Which is you year of birth?'**
  String get whichyear;

  /// No description provided for @whichmotherlang.
  ///
  /// In en, this message translates to:
  /// **'What is yor native language?'**
  String get whichmotherlang;

  /// No description provided for @recodingPromt.
  ///
  /// In en, this message translates to:
  /// **'Please make a recording of this word:'**
  String get recodingPromt;

  /// No description provided for @dacitDescription.
  ///
  /// In en, this message translates to:
  /// **'Choose option:'**
  String get dacitDescription;

  /// No description provided for @identWord.
  ///
  /// In en, this message translates to:
  /// **'Identify single words'**
  String get identWord;

  /// No description provided for @identSpeaker.
  ///
  /// In en, this message translates to:
  /// **'Identify speakers'**
  String get identSpeaker;

  /// No description provided for @additInfo.
  ///
  /// In en, this message translates to:
  /// **'Additional information'**
  String get additInfo;

  /// No description provided for @training.
  ///
  /// In en, this message translates to:
  /// **'Training sessions since last week'**
  String get training;

  /// No description provided for @recordings.
  ///
  /// In en, this message translates to:
  /// **'Recordings made'**
  String get recordings;

  /// No description provided for @minimalPairsDesc.
  ///
  /// In en, this message translates to:
  /// **'Are these two words the same?'**
  String get minimalPairsDesc;

  /// No description provided for @waitForRecording.
  ///
  /// In en, this message translates to:
  /// **'Waiting to record'**
  String get waitForRecording;

  /// No description provided for @pleaseEnterPassword.
  ///
  /// In en, this message translates to:
  /// **'Please enter your password'**
  String get pleaseEnterPassword;

  /// No description provided for @password.
  ///
  /// In en, this message translates to:
  /// **'Password'**
  String get password;

  /// No description provided for @pleaseEnterEmail.
  ///
  /// In en, this message translates to:
  /// **'Please enter your email'**
  String get pleaseEnterEmail;

  /// No description provided for @email.
  ///
  /// In en, this message translates to:
  /// **'Email'**
  String get email;

  /// No description provided for @signUp.
  ///
  /// In en, this message translates to:
  /// **'Sign up'**
  String get signUp;

  /// No description provided for @confirmPassword.
  ///
  /// In en, this message translates to:
  /// **'Confirm password'**
  String get confirmPassword;

  /// No description provided for @pleaseConfirmPassword.
  ///
  /// In en, this message translates to:
  /// **'Please confirm your password'**
  String get pleaseConfirmPassword;

  /// No description provided for @passwordsDontMatch.
  ///
  /// In en, this message translates to:
  /// **'Passwords don\'t match'**
  String get passwordsDontMatch;

  /// No description provided for @appID.
  ///
  /// In en, this message translates to:
  /// **'Your App-ID: '**
  String get appID;

  /// No description provided for @aboutText.
  ///
  /// In en, this message translates to:
  /// **'This is an app developed for a master thesis etc'**
  String get aboutText;

  /// No description provided for @invalidUserNorP.
  ///
  /// In en, this message translates to:
  /// **'Invalid username or password'**
  String get invalidUserNorP;

  /// No description provided for @login.
  ///
  /// In en, this message translates to:
  /// **'Login'**
  String get login;

  /// No description provided for @createAccount.
  ///
  /// In en, this message translates to:
  /// **'Create an account'**
  String get createAccount;

  /// No description provided for @recordThisWord.
  ///
  /// In en, this message translates to:
  /// **'Please make a recording to this word '**
  String get recordThisWord;

  /// No description provided for @role.
  ///
  /// In en, this message translates to:
  /// **'role'**
  String get role;
}

class _AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const _AppLocalizationsDelegate();

  @override
  Future<AppLocalizations> load(Locale locale) {
    return SynchronousFuture<AppLocalizations>(lookupAppLocalizations(locale));
  }

  @override
  bool isSupported(Locale locale) => <String>['de', 'en'].contains(locale.languageCode);

  @override
  bool shouldReload(_AppLocalizationsDelegate old) => false;
}

AppLocalizations lookupAppLocalizations(Locale locale) {


  // Lookup logic when only language code is specified.
  switch (locale.languageCode) {
    case 'de': return AppLocalizationsDe();
    case 'en': return AppLocalizationsEn();
  }

  throw FlutterError(
    'AppLocalizations.delegate failed to load unsupported locale "$locale". This is likely '
    'an issue with the localizations generation tool. Please file an issue '
    'on GitHub with a reproducible sample app and the gen-l10n configuration '
    'that was used.'
  );
}
