// ignore: unused_import
import 'package:intl/intl.dart' as intl;
import 'app_localizations.dart';

// ignore_for_file: type=lint

/// The translations for German (`de`).
class AppLocalizationsDe extends AppLocalizations {
  AppLocalizationsDe([String locale = 'de']) : super(locale);

  @override
  String get minimalPairs => 'Minimal Paare Trainieren';

  @override
  String get recordAudio => 'Audio Aufnehmen';

  @override
  String get recordAudioDesc => 'Nehme Hörbeispiele auf';

  @override
  String get speakerDisambiguation => 'Sprecher Unterscheiden';

  @override
  String get settings => 'Einstellungen';

  @override
  String get about => 'Über';

  @override
  String get welcome => 'Willkommen zu Dacit';

  @override
  String get whichdialect => 'Welchen Dialekt sprechen Sie?';

  @override
  String get whichyear => 'In welchem Jahr sind Sie geboren?';

  @override
  String get whichmotherlang => 'Was ist Ihre Muttersprache?';

  @override
  String get recodingPromt => 'Nehmen Sie das folgende Wort auf:';

  @override
  String get dacitDescription => 'Option auswählen:';

  @override
  String get identWord => 'Identifiziere einzelne Wörter';

  @override
  String get identSpeaker => 'Indentifiziere Sprecher';

  @override
  String get additInfo => 'Weitere Informationen';

  @override
  String get training => 'Trainingseinheiten seit letzter Woche';

  @override
  String get recordings => 'erstellte Aufnahmen';

  @override
  String get minimalPairsDesc => 'Sind diese beiden Worte gleich?';

  @override
  String get waitForRecording => 'Warte auf Aufnahme';

  @override
  String get pleaseEnterPassword => 'Geben sie bitte Ihr Passwort ein';

  @override
  String get password => 'Fügen Sie Ihre Zugangsdaten ein';

  @override
  String get pleaseEnterEmail => 'Geben Sie Ihre Email ein';

  @override
  String get email => 'Email';

  @override
  String get signUp => 'Account erstellen';

  @override
  String get confirmPassword => 'Passwort bestätigen';

  @override
  String get pleaseConfirmPassword => 'Bitte bestätigen Sie Ihr Passwort';

  @override
  String get passwordsDontMatch => 'Passwörter stimmen nicht überein';

  @override
  String get appID => 'Ihre App-ID: ';

  @override
  String get aboutText => 'Diese App wurde im Rahmen einer Masterarbeit entwickelt.';

  @override
  String get invalidUserNorP => 'Ungültiger Benutzername oder Passwort';

  @override
  String get login => 'Einloggen';

  @override
  String get createAccount => 'Account erstellen';

  @override
  String get recordThisWord => 'Nehme das folgende Wort auf: ';

  @override
  String get role => 'Rolle';
}
