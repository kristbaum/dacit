// ignore: unused_import
import 'package:intl/intl.dart' as intl;
import 'app_localizations.dart';

// ignore_for_file: type=lint

/// The translations for English (`en`).
class AppLocalizationsEn extends AppLocalizations {
  AppLocalizationsEn([String locale = 'en']) : super(locale);

  @override
  String get minimalPairs => 'Train Minimal Pairs';

  @override
  String get recordAudio => 'Record Audio';

  @override
  String get recordAudioDesc => 'Record new audio';

  @override
  String get speakerDisambiguation => 'Train Speaker Disambiguation';

  @override
  String get settings => 'Settings';

  @override
  String get about => 'About';

  @override
  String get welcome => 'Welcome to Dacit\na training app for people with cochlear implants';

  @override
  String get whichdialect => 'which dialect do you speak?';

  @override
  String get whichyear => 'Which is you year of birth?';

  @override
  String get whichmotherlang => 'What is yor native language?';

  @override
  String get recodingPromt => 'Please make a recording of this word:';

  @override
  String get dacitDescription => 'Choose option:';

  @override
  String get identWord => 'Identify single words';

  @override
  String get identSpeaker => 'Identify speakers';

  @override
  String get additInfo => 'Additional information';

  @override
  String get training => 'Training sessions since last week';

  @override
  String get recordings => 'Recordings made';

  @override
  String get minimalPairsDesc => 'Are these two words the same?';

  @override
  String get waitForRecording => 'Waiting to record';

  @override
  String get pleaseEnterPassword => 'Please enter your password';

  @override
  String get password => 'Password';

  @override
  String get pleaseEnterEmail => 'Please enter your email';

  @override
  String get email => 'Email';

  @override
  String get signUp => 'Sign up';

  @override
  String get confirmPassword => 'Confirm password';

  @override
  String get pleaseConfirmPassword => 'Please confirm your password';

  @override
  String get passwordsDontMatch => 'Passwords don\'t match';

  @override
  String get appID => 'Your App-ID: ';

  @override
  String get aboutText => 'This is an app developed for a master thesis etc';

  @override
  String get invalidUserNorP => 'Invalid username or password';

  @override
  String get login => 'Login';

  @override
  String get createAccount => 'Create an account';

  @override
  String get recordThisWord => 'Please make a recording to this word ';

  @override
  String get role => 'role';
}
