import 'package:dacit/pages/home_page.dart';
import 'package:dacit/pages/login_page.dart';
import 'package:dacit/pages/min_pairs_page.dart';
import 'package:dacit/pages/privacy_page.dart';
import 'package:dacit/pages/signup_page.dart';
import 'package:dacit/pages/stats_page.dart';
import 'package:dacit/pages/word_training_page.dart';
import 'package:dacit/services/login.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:dacit/l10n/app_localizations.dart';
import 'package:dacit/pages/settings_page.dart';
import 'package:dacit/pages/speaker_dis_page.dart';
import 'package:dacit/pages/about_page.dart';
import 'package:dacit/pages/record_page.dart';
import 'package:logging/logging.dart';

void main() async {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((record) {
    // ignore: avoid_print
    print('${record.level.name}: ${record.time}: ${record.message}');
  });
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);
  await getLogInData();

  runApp(const Dacit());
}

class Dacit extends StatelessWidget {
  const Dacit({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dacit',
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
        brightness: Brightness.light,
        //primaryColor: Colors.blueGrey,
        scaffoldBackgroundColor: Colors.blueGrey.shade50,
        fontFamily: 'Georgia',
        useMaterial3: true,
        appBarTheme: AppBarTheme(
          backgroundColor: Colors.blueGrey.shade50,
          elevation: 10,
        ),
        bottomAppBarTheme: BottomAppBarTheme(
          color: Colors.blueGrey.shade50,
        ),
        buttonTheme: const ButtonThemeData(
          //buttonColor: Colors.blueGrey,
          height: 300.0,
          minWidth: 500.0,
          //textTheme: ButtonTextTheme.primary,
        ),
        iconTheme: const IconThemeData(
          size: 50.0,
        ),
        iconButtonTheme: IconButtonThemeData(
            style: IconButton.styleFrom(
          backgroundColor: Colors.blueGrey.shade100,
        )),
        elevatedButtonTheme: ElevatedButtonThemeData(
            style: ElevatedButton.styleFrom(
          backgroundColor: Colors.blueGrey.shade100,
        )),
        textTheme: Theme.of(context).textTheme.apply(
              fontSizeFactor: 2.0,
              //fontSizeDelta: 5.0,
            ),
      ),
      home: const HomePage(),
      routes: {
        '/minpairs': (context) => const MinimalPairsPage(),
        '/wordtraining': (context) => const WordTrainingPage(),
        '/record': (context) => const RecordPage(),
        '/settings': (context) => const SettingsPage(),
        '/speakerdis': (context) => const SpeakerDisPage(),
        '/stats': (context) => const StatsPage(),
        '/about': (context) => const AboutPage(),
        '/privacy': (context) => const PrivacyPage(),
        '/login': (context) => const LoginPage(),
        '/signup': (context) => const SignupPage(),
      },
    );
  }
}
