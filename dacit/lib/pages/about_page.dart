import 'package:dacit/l10n/app_localizations.dart';
import 'package:flutter/material.dart';

class AboutPage extends StatelessWidget {
  const AboutPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: FittedBox(child: Text(AppLocalizations.of(context)!.about))),
        body: Center(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: RichText(
                text: const TextSpan(
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 30,
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text:
                          'Diese App wurde im Rahmen einer Masterarbeit an der LMU München entwickelt. ',
                    ),
                    TextSpan(
                      text:
                          'Sie versucht das Hörtraining von Personen mit CI-Implantat zu unterstützen, ',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    TextSpan(
                      text: 'durch den Einsatz von Sprachaufnahmen. ',
                    ),
                    TextSpan(
                      text:
                          'Ein Teil dieser Sprachaufnahmen wird direkt von Personen aus dem Umfeld der Nutzer*in erstellt.',
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
