import 'package:dacit/l10n/app_localizations.dart';
import 'package:dacit/services/globals.dart';
import 'package:dacit/services/stats.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:url_launcher/url_launcher.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});
  final String title = "Dacit";
  @override
  State<StatefulWidget> createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  bool _fetchNew = true;

  Future<StatsHome> _fetchStatsHome() async {
    final response = await http.get(
      Uri.parse('${baseDomain}api/stats?is_home=true'),
      headers: {
        "Authorization": "Token ${user.token}",
      },
    );

    _fetchNew = false;

    if (response.statusCode == 200) {
      log.info("Downloading new Stats");
      return StatsHome.fromJson(jsonDecode(utf8.decode(response.bodyBytes)));
    } else {
      throw Exception('Gerade sind keine Statistiken verfügbar');
    }
  }

  @override
  Widget build(BuildContext context) {
    bool isScreenWide = MediaQuery.of(context).size.width >= 1050;
    return Scaffold(
        drawer: drawer(context),
        appBar: AppBar(),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              const Image(image: AssetImage('assets/images/icon.png')),
              const Text(
                "Dacit",
                style: TextStyle(fontSize: 60),
              ),
              //OutlinedButton(onPressed: () {}, child: const Text("Start")),
              Builder(builder: (context) {
                return ElevatedButton(
                    onPressed: () {
                      Scaffold.of(context).openDrawer();
                    },
                    child: const Text("Start",
                        style: TextStyle(fontWeight: FontWeight.bold)));
              }),
              FutureBuilder<StatsHome>(
                future: _fetchNew ? _fetchStatsHome() : null,
                //TODO: Refresh on return navigation back to homepage
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    trainingCounter = snapshot.data!.trainingToday;
                    return Flex(
                        direction:
                            isScreenWide ? Axis.horizontal : Axis.vertical,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Card(
                            color: trainingCounter ~/ trainingRounds + 1 >= 4
                                ? Colors.greenAccent
                                : Colors.redAccent,
                            child: ConstrainedBox(
                              constraints: const BoxConstraints(
                                  maxWidth: 350, minHeight: 100),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                        "${trainingCounter ~/ trainingRounds}/4"),
                                    const FittedBox(
                                        child:
                                            Text("Trainingseinheiten heute")),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Card(
                            child: ConstrainedBox(
                              constraints: const BoxConstraints(
                                  maxWidth: 350, minHeight: 100),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    FittedBox(
                                      child: RichText(
                                        textAlign: TextAlign.center,
                                        text: TextSpan(children: [
                                          TextSpan(
                                            text: 'Fragebogen',
                                            style: const TextStyle(
                                                color: Colors.blue,
                                                fontSize: 26),
                                            recognizer: TapGestureRecognizer()
                                              ..onTap = () {
                                                launchUrl(Uri.parse(
                                                    "https://survey.ifkw.lmu.de/dacit/"));
                                              },
                                          ),
                                          const TextSpan(
                                            text: ' beantworten\n(nach Ende ihrer Benutzung)',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 26),
                                          ),
                                        ]),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Card(
                            child: ConstrainedBox(
                              constraints: const BoxConstraints(
                                  maxWidth: 350, minHeight: 100),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: FittedBox(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text("${snapshot.data!.numAudio}"),
                                      Text(AppLocalizations.of(context)!
                                          .recordings)
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          )
                        ]);
                  } else if (snapshot.hasError) {
                    return Text('${snapshot.error}');
                  }
                  return const CircularProgressIndicator();
                },
              ),
            ],
          ),
        ));
  }

  Widget drawer(BuildContext context) {
    return Drawer(
        child: ListView(
      padding: EdgeInsets.zero,
      children: [
        DrawerHeader(
          decoration: const BoxDecoration(
              //color: Colors.blue,
              ),
          child: Column(
            children: [
              const Image(
                  image: AssetImage('assets/images/icon.png'), height: 90),
              FittedBox(
                child: Text(AppLocalizations.of(context)!.dacitDescription,
                    style: const TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 24,
                    )),
              ),
            ],
          ),
        ),
        _tile(
            AppLocalizations.of(context)!.recordAudio,
            AppLocalizations.of(context)!.recordAudioDesc,
            "record",
            Icons.mic_rounded,
            context),
        _tile("Worte Üben", AppLocalizations.of(context)!.identWord,
            "wordtraining", Icons.model_training, context),
        _tile(
            AppLocalizations.of(context)!.speakerDisambiguation,
            AppLocalizations.of(context)!.identSpeaker,
            "speakerdis",
            Icons.record_voice_over,
            context),
        _tile(
            AppLocalizations.of(context)!.minimalPairs,
            "Unterscheide ähnliche Wörter",
            "minpairs",
            Icons.audiotrack,
            context),
        const Divider(
          thickness: 10,
        ),
        _tile("Statistiken", "", "stats", Icons.bar_chart, context),
        const SizedBox(
          height: 10,
        ),
        _tile(AppLocalizations.of(context)!.settings, "", "settings",
            Icons.settings, context),
        const SizedBox(
          height: 10,
        ),
        _tile("Datenschutz", "", "privacy", Icons.privacy_tip, context),
        const SizedBox(
          height: 10,
        ),
        _tile(AppLocalizations.of(context)!.about, "", "about", Icons.info,
            context),
      ],
    ));
  }

  ListTile _tile(String title, String subtitle, String path, IconData icon,
      BuildContext context) {
    return ListTile(
      title: Text(title,
          style: const TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 22,
          )),
      subtitle: subtitle == ""
          ? null
          : Text(subtitle,
              style: const TextStyle(
                fontSize: 20,
              )),
      leading: Icon(
        icon,
        //color: Colors.blue,
      ),
      onTap: () {
        Navigator.pop(context);
        Navigator.pushNamed(context, '/$path').then((value) => setState(() {
              _fetchNew = true;
            }));
      },
    );
  }
}
