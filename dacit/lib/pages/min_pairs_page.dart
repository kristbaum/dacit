import 'dart:convert';

import 'package:audioplayers/audioplayers.dart';
import 'package:dacit/services/globals.dart';
import 'package:dacit/services/min_pair.dart';
import 'package:flutter/material.dart';
import 'package:dacit/l10n/app_localizations.dart';
import 'package:http/http.dart' as http;

class MinimalPairsPage extends StatefulWidget {
  const MinimalPairsPage({super.key});

  @override
  State<StatefulWidget> createState() => _MinimalPairsPageState();
}

class _MinimalPairsPageState extends State<MinimalPairsPage> {
  late MinPair _activeMinPair;
  final AudioPlayer _firstAudioPlayer = AudioPlayer();
  final AudioPlayer _secondAudioPlayer = AudioPlayer();
  int _firstAudioRepeated = 0;
  int _secondAudioRepeated = 0;
  bool _answerLocked = false;
  bool _fetchNew = true;
  bool _correct = false;

  @override
  void initState() {
    super.initState();
    _firstAudioPlayer.setReleaseMode(ReleaseMode.stop);
    _secondAudioPlayer.setReleaseMode(ReleaseMode.stop);
  }

  Future<MinPair> _fetchMinPair() async {
    final response = await http.get(
      Uri.parse('${baseDomain}api/minpair'),
      headers: {
        "Authorization": "Token ${user.token}",
      },
    );

    if (response.statusCode == 200) {
      log.info("Downloading new Minpair");
      return MinPair.fromJson(jsonDecode(utf8.decode(response.bodyBytes)));
    } else {
      throw Exception('Gerade sind keine neuen Inhalte verfügbar, prüfen Sie ihre Internetverbindung');
    }
  }

  Future<void> _postMinPairAnswer(bool answerEqual) async {
    MinPairAnwser minPairAnswer = MinPairAnwser(_activeMinPair.id, answerEqual,
        _firstAudioRepeated, _secondAudioRepeated);
    log.info("trying to send result msg");
    log.info(json.encode(minPairAnswer));
    final response = await http.post(Uri.parse('${baseDomain}api/minpair/'),
        headers: {
          "Authorization": "Token ${user.token}",
          "content-type": "application/json",
        },
        body: json.encode(minPairAnswer));
    log.info("Successfully posted");
    if (response.statusCode == 200) {
      log.info("Send result successful");
    } else {
      throw Exception('Sending result not possible');
    }
  }

  void _displayResultAndContinue(bool answerEqual, MinPair minPair) {
    trainingCounter++;
    setState(() {
      _answerLocked = true;
      _fetchNew = false;
      _correct =
          (minPair.firstStimulus == minPair.secondStimulus && answerEqual) ||
              (minPair.firstStimulus != minPair.secondStimulus && !answerEqual);
    });
    log.info("Answer is: $_correct");
    _postMinPairAnswer(answerEqual);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: FittedBox(
              child: Text(AppLocalizations.of(context)!.minimalPairs)),
          bottom: PreferredSize(
              preferredSize: const Size.fromHeight(1),
              child: LinearProgressIndicator(
                value: ((100 / trainingRounds) *
                        (trainingCounter % trainingRounds)) /
                    100,
                minHeight: 8,
              )),
        ),
        bottomNavigationBar: BottomAppBar(
          color: _answerLocked
              ? _correct
                  ? Colors.greenAccent
                  : Colors.redAccent
              : null,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Visibility(
                visible: _answerLocked ? true : false,
                maintainSize: true,
                maintainAnimation: true,
                maintainState: true,
                child: Icon(
                  _correct ? Icons.check : Icons.close,
                ),
              ),
              ElevatedButton(
                onPressed: _answerLocked
                    ? () {
                        trainingCounter % trainingRounds == 0
                            ? Navigator.of(context).pop()
                            : setState(() {
                                _answerLocked = false;
                                _fetchNew = true;
                              });
                      }
                    : null,
                child: trainingCounter % trainingRounds == 0
                    ? const Text("Abschließen")
                    : const Text(
                        "Weiter",
                      ),
              ),
            ],
          ),
        ),
        body: Center(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
              Container(
                  padding: const EdgeInsets.all(15),
                  child: const Text(
                      "Gibt es einen Unterschied in den folgenden Aufnahmen:")),
              FutureBuilder<MinPair>(
                future: _fetchNew ? _fetchMinPair() : null,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    _activeMinPair = snapshot.data!;
                    log.info(_activeMinPair.firstAudio);
                    _firstAudioPlayer.setSourceUrl(
                        "${baseDomain}api/download/${_activeMinPair.firstAudio}");
                    _secondAudioPlayer.setSourceUrl(
                        "${baseDomain}api/download/${_activeMinPair.secondAudio}");
                    //return Text(minPair.category);
                    return Column(
                      children: [
                        addButtons(),
                        addSolution(),
                      ],
                    );
                  } else if (snapshot.hasError) {
                    //setState(() {});
                    return Text('${snapshot.error}');
                  }
                  return const CircularProgressIndicator();
                },
              ),
            ])));
  }

  Row addSolution() {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
      Flexible(
        child: Visibility(
          visible: _answerLocked,
          maintainSize: true,
          maintainAnimation: true,
          maintainState: true,
          child: Container(
            padding: const EdgeInsets.all(15),
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColorLight,
                borderRadius: const BorderRadius.all(Radius.circular(10.0))),
            child: Text(
              "\"${_activeMinPair.firstStimulus}\"",
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ),
      ),
      Flexible(
          child: Text(_activeMinPair.category.replaceAll("_", "-"),
              overflow: TextOverflow.ellipsis)),
      Flexible(
        child: Visibility(
          visible: _answerLocked,
          maintainSize: true,
          maintainAnimation: true,
          maintainState: true,
          child: Container(
            padding: const EdgeInsets.all(15),
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColorLight,
                borderRadius: const BorderRadius.all(Radius.circular(10.0))),
            child: Text(
              "\"${_activeMinPair.secondStimulus}\"",
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ),
      ),
    ]);
  }

  Row addButtons() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            // First play button
            IconButton(
              style: IconButton.styleFrom(
                padding: const EdgeInsets.all(15),
                shape: const CircleBorder(),
              ),
              icon: const Icon(Icons.play_arrow_rounded),
              iconSize: 100,
              onPressed: () {
                _firstAudioRepeated += 1;
                _secondAudioPlayer.stop();
                _firstAudioPlayer.stop();
                _firstAudioPlayer.resume();
              },
            ),
          ],
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // Answer is diffrent button
            ElevatedButton(
              onPressed: _answerLocked
                  ? null
                  : () {
                      _displayResultAndContinue(false, _activeMinPair);
                    },
              style: ElevatedButton.styleFrom(
                padding: const EdgeInsets.all(20),
                shape: const CircleBorder(),
              ),
              child: const Text(
                "≠",
                textScaleFactor: 3,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            const SizedBox(height: 20),
            // Answer is equal button
            ElevatedButton(
              onPressed: _answerLocked
                  ? null
                  : () {
                      _displayResultAndContinue(true, _activeMinPair);
                    },
              style: ElevatedButton.styleFrom(
                padding: const EdgeInsets.all(20),
                shape: const CircleBorder(),
              ),
              child: const Text(
                '=',
                textScaleFactor: 3,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        // Second play button
        Column(
          children: [
            IconButton(
              style: IconButton.styleFrom(
                padding: const EdgeInsets.all(15),
                shape: const CircleBorder(),
              ),
              icon: const Icon(Icons.play_arrow_rounded),
              iconSize: 100,
              onPressed: () {
                _secondAudioRepeated += 1;
                _firstAudioPlayer.stop();
                _secondAudioPlayer.stop();
                _secondAudioPlayer.resume();
              },
            ),
          ],
        ),
      ],
    );
  }
}
