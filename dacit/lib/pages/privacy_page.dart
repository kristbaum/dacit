// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class PrivacyPage extends StatelessWidget {
  const PrivacyPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: FittedBox(child: const Text("Datenschutz"))),
        body: SingleChildScrollView(
          child: RichText(
            text: TextSpan(
              style: TextStyle(
                color: Colors.black,
                fontSize: 30,
              ),
              children: const <TextSpan>[
                TextSpan(
                  text: 'Datenschutzerklärung\n',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                TextSpan(
                  text: 'Was geschieht mit den über mich erhobenen Daten?\n',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                TextSpan(
                  text: 'a) Allgemeine Informationen\n',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                TextSpan(
                  text:
                      'App-Performance: Während der Nutzung der Dacit-App werden Daten zur Leistung und Interaktion anonymisiert erfasst.\n',
                ),
                TextSpan(
                  text:
                      'Sprachaufnahmen: Die App ermöglicht es, Sprachaufnahmen zu erstellen, die gespeichert und von anderen Benutzern angehört werden können.\n',
                ),
                TextSpan(
                  text:
                      'Fragebogen: Der Fragebogen am Ende der Studie enthält Fragen zu ihrem Umgang mit der App und zu ihrem Hörvermögen, die anonymisiert abgefragt werden. Den Link zum Fragebogen wird beispielsweise über Dacit angeboten. Während der klinischen Prüfung werden persönliche Informationen von Ihnen anonymisiert erfasst und in den Studienunterlagen elektronisch gespeichert. Alle Sprachaufnahmen die von Personen aus ihrem Umfeld oder von Ihnen erstellt werden, werden nach Ablauf der Studie gelöscht.\n',
                ),
                TextSpan(
                  text: 'b) Rechtsgrundlage\n',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                TextSpan(
                  text:
                      'Rechtsgrundlage für die Datenverarbeitung ist Ihre informierte Einwilligung gemäß Art. 6 Abs. 1 Buchst. a und Art. 9 Abs. 2 Buchst. a der EU Datenschutzgrundverordnung (DSGVO) sowie § 29 Medizinprodukterecht-Durchführungsgesetz (MPDG).\n',
                ),
                TextSpan(
                  text: 'c) Verantwortlichkeit\n',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                TextSpan(
                  text:
                      'Sie haben das Recht, vom Verantwortlichen (s.u.) Auskunft über etwaige von Ihnen gespeicherten personenbezogenen Daten zu verlangen. Ebenfalls können Sie die Berichtigung unzutreffender Daten sowie die Löschung der Daten oder Einschränkung deren Verarbeitung verlangen. Der Verantwortliche für die studienbedingte Erhebung personenbezogener Daten ist: Maximilian Kristen max@kristenonline.de\n',
                ),
                TextSpan(
                  text: 'd) Zweck\n',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                TextSpan(
                  text:
                      'Mit Hilfe der erhobenen Daten soll die Sicherheit und Leistungsfähigkeit von Dacit klinisch untersucht werden.\n',
                ),
                TextSpan(
                  text: 'e) Ihre Rechte\n',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                TextSpan(
                  text:
                      'Sie haben grundsätzlich folgende Rechte bezüglich Ihrer personenbezogenen Daten, sofern dies nicht aufgrund einer zwischenzeitlich vorgenommenen Löschung der identifizierenden Merkmale zur Entschlüsselung technisch oder anderweitig gesetzlich unmöglich ist:\n',
                ),
                TextSpan(
                  text: 'Recht auf Widerruf ihrer Einwilligung\n',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                TextSpan(
                  text:
                      'So wie die Einwilligung zur Teilnahme an der klinischen Prüfung können Sie auch Ihre Einwilligung zur Verarbeitung der erhobenen Daten jederzeit widerrufen. Gemäß § 29 Nr. 2 MPDG dürfen im Falle eines Widerrufs Ihre gespeicherten Daten jedoch weiterverwendet werden, soweit dies erforderlich ist, um a) Ziele der klinischen Prüfung oder der sonstigen klinischen Prüfung zu verwirklichen oder nicht ernsthaft zu beeinträchtigen oder b) sicherzustellen, dass schutzwürdige Interessen der Prüfungsteilnehmer nicht beeinträchtigt werden. Im Falle eines Widerrufs Ihrer Einwilligung werden die verantwortlichen Stellen unverzüglich prüfen, inwieweit die gespeicherten Daten noch erforderlich sind. Nicht mehr benötigte Daten werden unverzüglich gelöscht.\n',
                ),
                TextSpan(
                  text:
                      'Sie haben weiterhin folgende Rechte: Recht auf Auskunft (inkl. unentgeltlicher Überlassung einer Kopie) über Ihre personenbezogenen Daten, die im Rahmen der klinischen Prüfung erhoben, verarbeitet oder ggf. an Dritte übermittelt werden. Recht auf Datenübertragung der zu Ihrer Person erhobenen Daten an Sie oder eine von Ihnen bestimmte Stelle. Recht auf Berichtigung unrichtiger personenbezogener Daten, auf Einschränkung der Verarbeitung und auf Widerspruch gegen die Nutzung der Daten.\n',
                ),
                TextSpan(
                  text: 'Wahrnehmung Ihrer Rechte\n',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                TextSpan(
                  text:
                      'Patienten-Information und -Einwilligung Wollen Sie von einem oder mehreren der genannten Rechte Gebrauch machen, kontaktieren Sie bitte Ihren Prüfer. Bei Anliegen zur Datenverarbeitung und zur Einhaltung der datenschutzrechtlichen Anforderungen können Sie sich auch an folgende Datenschutzbeauftragte wenden: \n\nHerr Dr.jur. Rolf Gemmeke\nLudwig-Maximilians-Universität München\nBehördlicher Datenschutzbeauftragter\nGeschwister-Scholl-Platz 1\n80539 München\nTel.: +49 89 2180 - 2414\nE-Mail: datenschutz@lmu.de\n\nSie haben außerdem ein Beschwerderecht bei einer Datenschutzaufsichtsbehörde. Sollten Sie Bedenken hinsichtlich des Umgangs mit Ihren personenbezogenen Daten haben, können Sie sich an folgende Stellen wenden: \n\nDer Landesbeauftragte für den Datenschutz und die Informationsfreiheit Bayern\nPostfach 22 12 19,\n80502 München\nWagmüllerstraße 18, 80538 München\nTel.: 089 212672-0\nFax: 089 212672-50\nE-Mail: poststelle@datenschutz-bayern.de\nInternet: https://www.datenschutz-bayern.de/\n\n',
                ),
                TextSpan(
                  text: 'f) Dauer der Speicherung der Daten:\n',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                TextSpan(
                  text:
                      'Die erhobenen Daten werden vom Entwickler Maximilian Kristen bis zur Auswertung der Daten und Beendigung der Abschlussarbeit (Masterarbeit) aufbewahrt. Danach werden Ihre Daten und Audioaufzeichnungen gelöscht.\n',
                ),
              ],
            ),
          ),
        ));
  }
}
