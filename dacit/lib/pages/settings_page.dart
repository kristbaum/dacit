// ignore_for_file: unused_import

import 'dart:convert';

import 'package:dacit/pages/login_page.dart';
import 'package:dacit/services/relationship.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

import 'package:dacit/services/globals.dart';
import 'package:dacit/services/settings/language_selection_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:dacit/l10n/app_localizations.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({super.key});

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  final TextEditingController _userIdController = TextEditingController();
  final TextEditingController _pinController = TextEditingController();

  void _logout(BuildContext context) async {
    final response = await http.post(
      Uri.parse('${baseDomain}dj-rest-auth/logout/'),
      headers: {
        "Authorization": "Token ${user.token}",
        'Content-Type': 'application/json',
      },
    );

    if (response.statusCode == 200 || response.statusCode == 401) {
      // Delete all stored data
      await storage.deleteAll();

      Future.delayed(Duration.zero, () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => const LoginPage()));
      });
    } else {
      // Logout failed
      setState(() {});
    }
  }

  Future<Relationship> _fetchRelationships() async {
    final response = await http.get(
      Uri.parse('${baseDomain}api/user_relationship'),
      headers: {
        "Authorization": "Token ${user.token}",
      },
    );

    if (response.statusCode == 200) {
      log.info("Downloading new Relationship");
      return Relationship.fromJson(jsonDecode(utf8.decode(response.bodyBytes)));
    } else {
      throw Exception(
          'Gerade sind keine neuen Inhalte verfügbar, prüfen Sie ihre Internetverbindung');
    }
  }

  Future<void> _postRelationship(String targetUser, int pin) async {
    RelationshipAnswer relationshipAnswer = RelationshipAnswer(targetUser, pin);
    log.info("trying to send result msg");
    log.info(json.encode(relationshipAnswer));
    final response =
        await http.post(Uri.parse('${baseDomain}api/user_relationship/'),
            headers: {
              "Authorization": "Token ${user.token}",
              "content-type": "application/json",
            },
            body: json.encode(relationshipAnswer));
    log.info("Successfully posted");
    if (response.statusCode == 201) {
      log.info("Send releationship successful");
      Future.delayed(Duration.zero, () {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text("Beziehung hinzugefügt!"),
          ),
        );
      });
    } else {
      log.warning("Did not complete");
      Future.delayed(Duration.zero, () {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text("Beziehung konnte nicht hinzugefügt werden"),
          ),
        );
      });
      throw Exception('Sending result not possible');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title:
              FittedBox(child: Text(AppLocalizations.of(context)!.settings))),
      body: SingleChildScrollView(
        child: Center(
            child: Column(
          children: <Widget>[
            FutureBuilder<Relationship>(
              future: _fetchRelationships(),
              builder:
                  (BuildContext context, AsyncSnapshot<Relationship> snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const CircularProgressIndicator();
                } else if (snapshot.hasError) {
                  return Text('Error: ${snapshot.error}');
                } else {
                  return Column(children: [
                    _addUserRelationshipCard(snapshot.data!),
                    const SizedBox(
                      height: 10,
                    ),
                    _addTargetUserRelationshipCard(snapshot.data!)
                  ]);
                  // Display other relationship data here
                }
              },
            ),
            const SizedBox(
              height: 10,
            ),
            Card(
                child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  leading: Icon(Icons.lock_person_outlined),
                  title: Text('Zugangsdaten'),
                  subtitle: Text(
                      'Kopieren Sie Ihre Zugangsdaten für Login auf anderen Geräten'),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    TextButton(
                      child: const Text('Kopieren'),
                      onPressed: () {
                        Clipboard.setData(ClipboardData(
                                text: "${user.username}:${user.password}"))
                            .then((_) {
                          ScaffoldMessenger.of(context).showSnackBar(
                              const SnackBar(
                                  content: Text('Zugangsdaten kopiert')));
                        });
                      },
                    ),
                  ],
                ),
              ],
            )),
            const SizedBox(
              height: 10,
            ),
            Card(
                child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                    leading: Icon(Icons.login_rounded),
                    title: Text('Login'),
                    subtitle: Text(
                        'Loggen Sie sich mit den Zugangsdaten eines anderen Gerätes ein')),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    TextButton(
                      child: const Text('Login'),
                      onPressed: () {
                        Future.delayed(Duration.zero, () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => const LoginPage()));
                        });
                      },
                    ),
                  ],
                ),
              ],
            )),
            const SizedBox(
              height: 10,
            ),
          ],
        )),
      ),
    );
  }

  Card _addUserRelationshipCard(Relationship relationship) {
    return Card(
        child: Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        const ListTile(
          leading: Icon(Icons.input_rounded),
          title: Text('Audio empfangen'),
          subtitle: Text(
              'Teilen Sie diese Daten mit Ihren Personen aus Ihrem Umfeld, damit diese Ihnen Audio senden können'),
        ),
        Text("Anzahl Bezugspersonen: ${relationship.receivesFrom.length}"),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text("ID: "),
            SelectableText(
                "${user.username.substring(0, 3)}-${user.username.substring(3, 6)}-${user.username.substring(6, 9)}"),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text("Pin: "),
            SelectableText("${relationship.pin}"),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            TextButton(
              child: const Text('Kopieren'),
              onPressed: () {
                Clipboard.setData(ClipboardData(
                        text:
                            "Besuchen Sie https://dacit.kristbaum.org und geben Sie unter Einstellungen diese ID: ${user.username} und diesen PIN an: ${relationship.pin}"))
                    .then((_) {
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      content: Text('Mitteilung in Zwischenspeicher kopiert')));
                });
              },
            ),
          ],
        ),
      ],
    ));
  }

  Card _addTargetUserRelationshipCard(Relationship relationship) {
    return Card(
        child: Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        ListTile(
          leading: const Icon(Icons.share),
          title: const Text('Audio teilen'),
          subtitle: Text(
              'Geben Sie ID und Pin einer Bezugsperson ein, um Sie mit Ihren Aufnahmen trainieren zu lassen, Aktuell: ${relationship.providesFor.length}'),
        ),
        SizedBox(
          width: 200,
          child: TextFormField(
            //keyboardType: TextInputType.number,
            controller: _userIdController,
            decoration: const InputDecoration(
              labelText: "ID eingeben",
            ),
            validator: (value) {
              if (value == null || value.isEmpty) {
                return "ID eingeben";
              }
              return null;
            },
          ),
        ),
        SizedBox(
          width: 200,
          child: TextFormField(
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.digitsOnly
            ],
            controller: _pinController,
            decoration: const InputDecoration(
              labelText: "Pin eingeben",
            ),
            validator: (value) {
              if (value == null || value.isEmpty) {
                return "Pin eingeben";
              }
              return null;
            },
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            TextButton(
              child: const Text('Senden'),
              onPressed: () {
                setState(() {});
                String userId = _userIdController.text.replaceAll('-', '');
                log.info("$userId pin: ${_pinController.text}");
                _postRelationship(userId, int.parse(_pinController.text));
              },
            ),
          ],
        ),
      ],
    ));
  }
}
