import 'dart:convert';
import 'package:audioplayers/audioplayers.dart';
import 'package:dacit/services/globals.dart';
import 'package:dacit/services/speaker_dis.dart';
import 'package:flutter/material.dart';
import 'package:dacit/l10n/app_localizations.dart';
import 'package:http/http.dart' as http;

class SpeakerDisPage extends StatefulWidget {
  const SpeakerDisPage({super.key});

  @override
  State<StatefulWidget> createState() => _SpeakerDisPageState();
}

class _SpeakerDisPageState extends State<SpeakerDisPage> {
  late SpeakerDis _activeSpeakerDis;
  final AudioPlayer _firstAudioPlayer = AudioPlayer();
  final AudioPlayer _secondAudioPlayer = AudioPlayer();
  int _firstAudioRepeated = 0;
  int _secondAudioRepeated = 0;
  bool _answerLocked = false;
  bool _fetchNew = true;
  bool _correct = false;

  @override
  void initState() {
    super.initState();
    _firstAudioPlayer.setReleaseMode(ReleaseMode.stop);
    _secondAudioPlayer.setReleaseMode(ReleaseMode.stop);
  }

  Future<SpeakerDis> _fetchSpeakerDis() async {
    final response = await http.get(
      Uri.parse('${baseDomain}api/speakerdis/'),
      headers: {
        "Authorization": "Token ${user.token}",
      },
    );

    if (response.statusCode == 200) {
      log.info("Downloading new SpeakerDis");
      log.info(response.body);
      log.info(jsonDecode(utf8.decode(response.bodyBytes)));
      return SpeakerDis.fromJson(jsonDecode(utf8.decode(response.bodyBytes)));
    } else {
      throw Exception(
          'Gerade sind keine neuen Inhalte verfügbar, prüfen Sie ihre Internetverbindung');
    }
  }

  Future<void> _postSpeakerDisAnswer(bool isAnswerEqual) async {
    SpeakerDisAnswer speakerDisAnswer = SpeakerDisAnswer(
        _activeSpeakerDis.speakerDisId,
        _firstAudioRepeated,
        _secondAudioRepeated,
        isAnswerEqual);
    log.info("trying to send result msg");
    log.info(json.encode(speakerDisAnswer));
    final response = await http.post(Uri.parse('${baseDomain}api/speakerdis/'),
        headers: {
          "Authorization": "Token ${user.token}",
          "content-type": "application/json",
        },
        body: json.encode(speakerDisAnswer));
    log.info("Successfully posted");
    if (response.statusCode == 200) {
      log.info("Send result successful");
    } else {
      throw Exception('Sending result not possible');
    }
  }

  void _displayResultAndContinue(bool answerEqual, SpeakerDis speakerDis) {
    trainingCounter++;
    setState(() {
      _answerLocked = true;
      _fetchNew = false;
      _correct = (speakerDis.isSameSpeaker == true && answerEqual) ||
          (speakerDis.isSameSpeaker == false && !answerEqual);
    });
    _postSpeakerDisAnswer(answerEqual);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: FittedBox(
              child: Text(AppLocalizations.of(context)!.speakerDisambiguation)),
          bottom: PreferredSize(
              preferredSize: const Size.fromHeight(1),
              child: LinearProgressIndicator(
                value: ((100 / trainingRounds) *
                        (trainingCounter % trainingRounds)) /
                    100,
                minHeight: 8,
              )),
        ),
        bottomNavigationBar: BottomAppBar(
          color: _answerLocked
              ? _correct
                  ? Colors.greenAccent
                  : Colors.redAccent
              : null,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Visibility(
                visible: _answerLocked ? true : false,
                maintainSize: true,
                maintainAnimation: true,
                maintainState: true,
                child: Icon(
                  _correct ? Icons.check : Icons.close,
                ),
              ),
              ElevatedButton(
                onPressed: _answerLocked
                    ? () {
                        trainingCounter % trainingRounds == 0
                            ? Navigator.of(context).pop()
                            : setState(() {
                                _answerLocked = false;
                                _fetchNew = true;
                              });
                      }
                    : null,
                child: trainingCounter % trainingRounds == 0
                    ? const Text("Abschließen")
                    : const Text(
                        "Weiter",
                      ),
              ),
            ],
          ),
        ),
        body: Center(
            child: Container(
                padding: const EdgeInsets.all(20.0),
                decoration: BoxDecoration(
                    color: Theme.of(context).secondaryHeaderColor,
                    borderRadius:
                        const BorderRadius.all(Radius.circular(10.0))),
                child: FutureBuilder<SpeakerDis>(
                  future: _fetchNew ? _fetchSpeakerDis() : null,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      _activeSpeakerDis = snapshot.data!;
                      log.info(_activeSpeakerDis.firstAudio);
                      _firstAudioPlayer
                          .setSourceUrl(_activeSpeakerDis.firstAudio);
                      _secondAudioPlayer
                          .setSourceUrl(_activeSpeakerDis.secondAudio);
                      log.info(_activeSpeakerDis.firstAudio);
                      return addButtons(context);
                    } else if (snapshot.hasError) {
                      //setState(() {});
                      return Text('${snapshot.error}');
                    }
                    return const CircularProgressIndicator();
                  },
                ))));
  }

  Column addButtons(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
              padding: const EdgeInsets.all(20.0),
              child: const Text("Spricht hier die gleiche Person?")),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              // First play button
              Column(
                children: [
                  IconButton(
                    style: IconButton.styleFrom(
                      padding: const EdgeInsets.all(15),
                      shape: const CircleBorder(),
                    ),
                    icon: const Icon(Icons.play_arrow_rounded),
                    iconSize: 100,
                    onPressed: () {
                      _firstAudioRepeated += 1;
                      _secondAudioPlayer.stop();
                      _firstAudioPlayer.stop();
                      _firstAudioPlayer.resume();
                    },
                  ),
                  const SizedBox(height: 5),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // Answer is diffrent button
                  ElevatedButton(
                    onPressed: _answerLocked
                        ? null
                        : () {
                            _displayResultAndContinue(false, _activeSpeakerDis);
                          },
                    style: ElevatedButton.styleFrom(
                      padding: const EdgeInsets.all(20),
                      shape: const CircleBorder(),
                    ),
                    child: const Text(
                      "≠",
                      textScaleFactor: 3,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  // Answer is equal button
                  ElevatedButton(
                    onPressed: _answerLocked
                        ? null
                        : () {
                            _displayResultAndContinue(true, _activeSpeakerDis);
                          },
                    style: ElevatedButton.styleFrom(
                      padding: const EdgeInsets.all(20),
                      shape: const CircleBorder(),
                    ),
                    child: const Text(
                      '=',
                      textScaleFactor: 3,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
              // Second play button
              Column(
                children: [
                  IconButton(
                    style: IconButton.styleFrom(
                      padding: const EdgeInsets.all(15),
                      shape: const CircleBorder(),
                    ),
                    icon: const Icon(Icons.play_arrow_rounded),
                    iconSize: 100,
                    onPressed: () {
                      _secondAudioRepeated += 1;
                      _firstAudioPlayer.stop();
                      _secondAudioPlayer.stop();
                      _secondAudioPlayer.resume();
                    },
                  ),
                  const SizedBox(height: 5),
                ],
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Flexible(
                child: Visibility(
                  visible: _answerLocked,
                  maintainSize: true,
                  maintainAnimation: true,
                  maintainState: true,
                  child: Container(
                    padding: const EdgeInsets.all(15),
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColorLight,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10.0))),
                    child: Column(
                      children: [
                        const Text("Person 1:"),
                        Text("\"${_activeSpeakerDis.firstStimulus.stimulus}\"",
                            overflow: TextOverflow.ellipsis, maxLines: 2),
                      ],
                    ),
                  ),
                ),
              ),
              Flexible(
                child: Visibility(
                  visible: _answerLocked,
                  maintainSize: true,
                  maintainAnimation: true,
                  maintainState: true,
                  child: Container(
                    padding: const EdgeInsets.all(15),
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColorLight,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10.0))),
                    child: Column(
                      children: [
                        _activeSpeakerDis.isSameSpeaker
                            ? const Text("Person 1:")
                            : const Text("Person 2:"),
                        Text(
                          "\"${_activeSpeakerDis.secondStimulus.stimulus}\"",
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          )
        ]);
  }
}
