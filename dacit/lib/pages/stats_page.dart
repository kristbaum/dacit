import 'dart:convert';

import 'package:dacit/services/stats.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../services/globals.dart';

class StatsPage extends StatefulWidget {
  const StatsPage({super.key});

  @override
  StatsPageState createState() => StatsPageState();
}

class StatsPageState extends State<StatsPage> {
  Future<Stats>? _statsFuture;
  List<Color> gradientColors = [
    const Color(0xff23b6e6),
    const Color(0xff02d39a),
  ];
  bool showAllTimeStats = false;

  @override
  void initState() {
    super.initState();
    _statsFuture = _fetchStats();
  }

  Future<Stats> _fetchStats() async {
    final response = await http.get(
      Uri.parse('${baseDomain}api/stats/'),
      headers: {
        "Authorization": "Token ${user.token}",
      },
    );

    if (response.statusCode == 200) {
      log.info("Downloading new Stats");
      log.info(response.body);
      return Stats.fromJson(jsonDecode(utf8.decode(response.bodyBytes)));
    } else {
      throw Exception(
          'Gerade sind keine neuen Inhalte verfügbar, prüfen Sie ihre Internetverbindung');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const FittedBox(child: Text('Statistiken')),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text("7 Tage "),
                  Switch(
                      value: showAllTimeStats,
                      onChanged: (bool value) {
                        setState(() {
                          showAllTimeStats = value;
                        });
                      }),
                  const Text(" Gesamt"),
                ],
              ),
            ),
            FutureBuilder<Stats>(
              future: _statsFuture,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                } else if (snapshot.hasError) {
                  return Center(
                    child: Text('Failed to load stats${snapshot.error}'),
                  );
                } else {
                  //final stats = snapshot.requireData;
                  final stats = snapshot.data!;
                  log.info(stats.numMinpair);
                  return Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        //TODO: Add charts of weekly performance and daily performance using fl_chart
                        //TODO: Add German translation
                        children: [
                          // Card(
                          //   child: Column(
                          //     children: [
                          //       const ListTile(
                          //         title: Text('Wöchentliche Performance'),
                          //       ),
                          //       Padding(
                          //         padding: const EdgeInsets.all(16.0),
                          //         child: Column(
                          //           children: [
                          //             const SizedBox(height: 32.0),
                          //             SizedBox(
                          //               width: 300,
                          //               height: 300,
                          //               child: LineChart(
                          //                 getweeklyChart(
                          //                     stats.trainingMinPairweek,
                          //                     stats.trainingWordTrainingweek,
                          //                     stats.trainingSpeakerDisweek),
                          //               ),
                          //             ),
                          //           ],
                          //         ),
                          //       ),
                          //     ],
                          //   ),
                          // ),
                          Card(
                            child: Column(
                              children: [
                                ListTile(
                                  title: const Text('Worttraining'),
                                  trailing: showAllTimeStats
                                      ? Text(
                                          '${stats.numWordtraining} zusammen')
                                      : Text(
                                          '${stats.numWordtrainingWeek} zusammen'),
                                ),
                                showAllTimeStats
                                    ? addPieChart(stats.numWordtraining,
                                        stats.numCorrectWordtraining)
                                    : addPieChart(stats.numWordtrainingWeek,
                                        stats.numCorrectWordtrainingWeek),
                                //addPieChart(stats.numWordtraining,
                                //    stats.numCorrectWordtraining),
                              ],
                            ),
                          ),
                          Card(
                            child: Column(
                              children: [
                                ListTile(
                                    title: const Text('Sprecherunterscheidung'),
                                    trailing: Text(showAllTimeStats
                                        ? '${stats.numSpeakerDis} zusammen'
                                        : '${stats.numSpeakerDisWeek} zusammen')),
                                showAllTimeStats
                                    ? addPieChart(stats.numSpeakerDis,
                                        stats.numCorrectSpeakerDis)
                                    : addPieChart(stats.numSpeakerDisWeek,
                                        stats.numCorrectSpeakerDisWeek),
                              ],
                            ),
                          ),
                          Card(
                            child: Column(children: [
                              ListTile(
                                  title: const Text('Minimal Paare'),
                                  trailing: showAllTimeStats
                                      ? Text('${stats.numMinpair} zusammen')
                                      : Text(
                                          '${stats.numMinpairWeek} zusammen')),
                              showAllTimeStats
                                  ? addPieChart(
                                      stats.numMinpair, stats.numCorrectMinpair)
                                  : addPieChart(stats.numMinpairWeek,
                                      stats.numCorrectMinpairWeek),
                            ]),
                          ),
                        ],
                      ),
                    ),
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  SizedBox addPieChart(int num, int numCorrect) {
    return SizedBox(
      width: 300,
      height: 300,
      child: num > 0
          ? PieChart(
              PieChartData(
                sections: [
                  PieChartSectionData(
                    value: num.toDouble() - numCorrect.toDouble(),
                    color: Colors.red,
                    radius: 50,
                  ),
                  PieChartSectionData(
                    value: numCorrect.toDouble(),
                    color: Colors.green,
                    radius: 60,
                  ),
                ],
              ),
            )
          : const Center(child: Text('Noch keine Ergebnisse')),
    );
  }

  LineChartData getweeklyChart(
      List<FlSpot> trainingMinPairweek,
      List<FlSpot> trainingWordTrainingweek,
      List<FlSpot> trainingSpeakerDisweek) {
    return LineChartData(
      lineTouchData: const LineTouchData(enabled: false),
      lineBarsData: [
        LineChartBarData(
          spots: trainingMinPairweek,
          dotData: const FlDotData(show: false),
          isCurved: true,
          barWidth: 2,
          color: Colors.purple,
        ),
        LineChartBarData(
            spots: trainingWordTrainingweek,
            dotData: const FlDotData(show: false),
            isCurved: true,
            barWidth: 2,
            color: Colors.blue),
        LineChartBarData(
          spots: trainingSpeakerDisweek,
          dotData: const FlDotData(show: false),
          isCurved: true,
          barWidth: 2,
          color: Colors.brown,
        ),
      ],
      titlesData: const FlTitlesData(
        show: true,
        rightTitles: AxisTitles(
          sideTitles: SideTitles(showTitles: false),
        ),
        topTitles: AxisTitles(sideTitles: SideTitles(showTitles: false)),
        bottomTitles: AxisTitles(sideTitles: SideTitles(showTitles: false)),
        leftTitles:
            AxisTitles(sideTitles: SideTitles(showTitles: true, interval: 2)),
      ),
      borderData: FlBorderData(
        show: true,
        border: Border.all(color: Colors.grey),
      ),
      maxX: trainingMinPairweek.length.toDouble(),
    );
  }
}
