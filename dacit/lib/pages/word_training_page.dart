import 'package:audioplayers/audioplayers.dart';
import 'package:dacit/services/globals.dart';
import 'package:dacit/services/word_training.dart';
import 'package:dacit/services/word_training_api.dart';
import 'package:flutter/material.dart';

class WordTrainingPage extends StatefulWidget {
  const WordTrainingPage({super.key});

  @override
  State<StatefulWidget> createState() => _WordTrainingPageState();
}

class _WordTrainingPageState extends State<WordTrainingPage> {
  late WordTraining activeWordTraining;
  final AudioPlayer _audioPlayer = AudioPlayer();
  int audioRepeated = 0;
  bool _answerLocked = false;
  bool _fetchNew = true;
  int _selectedChip = 100;
  bool _correct = false;

  @override
  void initState() {
    super.initState();
    _audioPlayer.setReleaseMode(ReleaseMode.stop);
  }

  void _displayResultAndContinue(
      bool selected, var index, WordTraining wordTraining) {
    trainingCounter++;
    setState(() {
      _answerLocked = true;
      _fetchNew = false;
      _correct =
          wordTraining.selectedStimulus.id == wordTraining.allAnswers[index].id;
      _selectedChip = selected ? index : null;
    });
    postWordTrainingAnswer(index, activeWordTraining, audioRepeated);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const FittedBox(child: Text("Wort Training")),
          bottom: PreferredSize(
              preferredSize: const Size.fromHeight(1),
              child: LinearProgressIndicator(
                value: ((100 / trainingRounds) *
                        (trainingCounter % trainingRounds)) /
                    100,
                minHeight: 8,
              )),
        ),
        bottomNavigationBar: BottomAppBar(
          color: _answerLocked
              ? _correct
                  ? Colors.greenAccent
                  : Colors.redAccent
              : null,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Visibility(
                visible: _answerLocked ? true : false,
                maintainSize: true,
                maintainAnimation: true,
                maintainState: true,
                child: Icon(
                  _correct ? Icons.check : Icons.close,
                ),
              ),
              ElevatedButton(
                onPressed: _answerLocked
                    ? () {
                        log.info("Training counter: $trainingCounter");
                        trainingCounter % trainingRounds == 0
                            ? (Future.delayed(Duration.zero, () {
                                Navigator.of(context).pop();
                                //Navigator.of(context).push(MaterialPageRoute(
                                //    builder: (context) => const HomePage())
                              }))
                            : setState(() {
                                _answerLocked = false;
                                _selectedChip = 100;
                                _fetchNew = true;
                              });
                      }
                    : null,
                child: trainingCounter % trainingRounds == 0
                    ? const Text("Abschließen")
                    : const Text("Weiter"),
              ),
            ],
          ),
        ),
        body: Center(
            child: Container(
                padding: const EdgeInsets.all(25.0),
                decoration: BoxDecoration(
                    color: Theme.of(context).secondaryHeaderColor,
                    borderRadius:
                        const BorderRadius.all(Radius.circular(10.0))),
                child: FutureBuilder<WordTraining>(
                  future: _fetchNew ? fetchWordTraining() : null,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      activeWordTraining = snapshot.data!;
                      _audioPlayer
                          .setSourceUrl(activeWordTraining.selectedAudio);
                      return addButtons();
                    } else if (snapshot.hasError) {
                      //setState(() {});
                      return Text('${snapshot.error}');
                    }
                    return const CircularProgressIndicator();
                  },
                ))));
  }

  Column addButtons() {
    return Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
      Container(
          padding: const EdgeInsets.all(15),
          child: const Text("Welches Wort wurde gesprochen?")),
      IconButton(
        icon: const Icon(Icons.play_arrow_rounded),
        iconSize: 100,
        onPressed: () {
          audioRepeated += 1;
          _audioPlayer.stop();
          _audioPlayer.resume();
        },
      ),
      Wrap(
        spacing: 8,
        runSpacing: 4,
        children: List<Widget>.generate(
          activeWordTraining.allAnswers.length,
          (int index) {
            return ChoiceChip(
              label: Text(activeWordTraining.allAnswers[index].stimulus),
              onSelected: _answerLocked
                  ? null
                  : (bool selected) {
                      _displayResultAndContinue(
                          selected, index, activeWordTraining);
                    },
              showCheckmark: false,
              selected: _selectedChip == index,
            );
          },
        ),
      )
    ]);
  }
}
