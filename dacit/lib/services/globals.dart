import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:logging/logging.dart';

final log = Logger('DacitLogger');
const int trainingRounds = 15;
//int trainingCounter = 0;
int trainingCounter = 0;

const String baseDomain = String.fromEnvironment('DACIT_API',
    defaultValue: 'http://localhost:8000/'); // TODO:

const storage = FlutterSecureStorage();
AppUser user = AppUser();

class AppUser {
  String username;
  String password;
  String token;
  String language;

  AppUser(
      {this.username = "User",
      this.token = "",
      this.password = "",
      this.language = "de"});
}
