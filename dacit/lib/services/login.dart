import 'dart:convert';
import 'dart:math';
import 'package:http/http.dart' as http;
import 'package:dacit/services/globals.dart';

Future<AppUser> getLogInData() async {
  final username = await storage.read(key: "username");
  final token = await storage.read(key: "token");
  final password = await storage.read(key: "password");
  final language = await storage.read(key: "language");

  if (token == null ||
      username == null ||
      password == null ||
      language == null) {
    log.info("Token not found, starting signup process");

    String newUsername = (100000000 + Random().nextInt(900000000)).toString();
    final randomSecure = Random.secure();
    var values = List<int>.generate(64, (i) => randomSecure.nextInt(255));
    String newPassword = base64UrlEncode(values);
    String newLanguage = "de";

    // Send a POST request to the signup API endpoint
    final response = await http.post(
      Uri.parse('${baseDomain}dj-rest-auth/registration/'),
      headers: {
        'Content-Type': 'application/json',
      },
      body: json.encode({
        'username': newUsername,
        'password1': newPassword,
        'password2': newPassword,
      }),
    );

    if (response.statusCode == 204) {
      log.info("SignUp successfull, try login next");

      // Send a POST request to the login API endpoint
      final response = await http.post(
        Uri.parse('${baseDomain}dj-rest-auth/login/'),
        headers: {
          'Content-Type': 'application/json',
        },
        body: json.encode({
          'username': newUsername,
          'password': newPassword,
        }),
      );

      if (response.statusCode == 200) {
        // Login successful
        final newToken = json.decode(response.body)['key'];
        await storage.write(key: 'username', value: newUsername);
        await storage.write(key: 'token', value: newToken);
        await storage.write(key: 'password', value: newPassword);
        await storage.write(key: 'language', value: newLanguage);
        user.username = newUsername;
        user.password = newPassword;
        user.token = newToken;
        user.language = newLanguage;
        return user;
      } else {
        // Login failed
        log.warning("Login after signup failed");
      }
    } else {
      // Signup failed
      log.warning("Signup failed");
    }
    throw "No token in storage";
  } else {
    //Test if token is still active
    log.warning("Token found, testing if token is still active");
    if (await isTokenActive(token)) {
      log.warning("Token is still active");
    } else {
      log.warning("Token is not active anymore, trying to login again");
      // Send a POST request to the login API endpoint
      final response = await http.post(
        Uri.parse('${baseDomain}dj-rest-auth/login/'),
        headers: {
          'Content-Type': 'application/json',
        },
        body: json.encode({
          'username': username,
          'password': password,
        }),
      );

      if (response.statusCode == 200) {
        // Login successful
        final newToken = json.decode(response.body)['key'];
        await storage.write(key: 'token', value: newToken);
        user.token = newToken;
        return user;
      } else {
        // Login failed
        log.warning("Login after finding inactive token failed");
        await storage.deleteAll();
        return getLogInData();
      }
    }
    user.username = username;
    user.password = password;
    user.token = token;
    user.language = language;
    return user;
  }
}

Future<bool> isTokenActive(String token) async {
  final response = await http.get(
    Uri.parse('${baseDomain}dj-rest-auth/user/'),
    headers: {'Authorization': 'Token $token'},
  );

  return response.statusCode == 200;
}
