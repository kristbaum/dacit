class MinPair {
  final int id;
  final String category;
  final String firstStimulus;
  final Uri firstAudio;
  final String secondStimulus;
  final Uri secondAudio;

  const MinPair(
      {required this.id,
      required this.category,
      required this.firstStimulus,
      required this.firstAudio,
      required this.secondStimulus,
      required this.secondAudio});

  factory MinPair.fromJson(Map<String, dynamic> json) {
    return MinPair(
      id: json['min_pair_round_id'],
      category: json['category'],
      firstStimulus: json["first_stimulus"],
      firstAudio: Uri.parse(json["first_audio"]),
      secondStimulus: json["second_stimulus"],
      secondAudio: Uri.parse(json["second_audio"]),
    );
  }
}

class MinPairAnwser {
  final int id;
  final bool answerEqual;
  final int firstAudioRepeated;
  final int secondAudioRepeated;

  MinPairAnwser(this.id, this.answerEqual, this.firstAudioRepeated,
      this.secondAudioRepeated);

  Map<dynamic, dynamic> toJson() => {
        'min_pair_round_id': id,
        'answer_equal': answerEqual,
        'first_audio_repeated': firstAudioRepeated,
        'second_audio_repeated': secondAudioRepeated,
      };
}
