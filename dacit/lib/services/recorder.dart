import 'dart:async';

import 'package:dacit/services/globals.dart';
import 'package:flutter/material.dart';
import 'package:record/record.dart';

import 'package:dacit/services/platform/audio_recorder_platform.dart';

class DacitRecorder extends StatefulWidget {
  final void Function(String path) onStop;

  const DacitRecorder({super.key, required this.onStop});

  @override
  State<DacitRecorder> createState() => _DacitRecorderState();
}

class _DacitRecorderState extends State<DacitRecorder> with AudioRecorderMixin {
  int _recordDuration = 0;
  Timer? _timer;
  late final AudioRecorder _audioRecorder;
  StreamSubscription<RecordState>? _recordSub;
  RecordState _recordState = RecordState.stop;
  StreamSubscription<Amplitude>? _amplitudeSub;

  @override
  void initState() {
    _audioRecorder = AudioRecorder();
    _recordSub = _audioRecorder.onStateChanged().listen((recordState) {
      setState(() => _recordState = recordState);
    });
    super.initState();
  }

  Future<void> _start() async {
    log.info("Start recording");
    // TODO: Check if audio devices avaialble, and warn if not
    try {
      if (await _audioRecorder.hasPermission()) {
        const encoder = AudioEncoder.wav;

        // We don't do anything with this but printing
        final isSupported = await _audioRecorder.isEncoderSupported(
          encoder,
        );

        log.info('${encoder.name} supported: $isSupported');

        final devs = await _audioRecorder.listInputDevices();
        log.info(devs.toString());

        if (devs.isEmpty) {
          throw Exception("No audio devices found");
        }

        const config = RecordConfig(encoder: encoder);

        // await _audioRecorder.start(config);
        // _recordDuration = 0;

        // Record to file
        await recordFile(_audioRecorder, config);

        // Record to stream
        // await recordStream(_audioRecorder, config);

        _recordDuration = 0;
        _startTimer();
      } else {
        Future.delayed(Duration.zero, () {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text(
                  "Keine Berechtigung zum Aufnehmen, oder kein Aufnahmegerät vorhanden"),
            ),
          );
        });
        throw Exception(
            "Keine Berechtigung zum Aufnehmen, oder kein Aufnahmegerät vorhanden");
      }
    } catch (e) {
      log.severe(e);
    }
  }

  Future<void> _stop() async {
    _timer?.cancel();
    _recordDuration = 0;

    final path = await _audioRecorder.stop();

    if (path != null) {
      widget.onStop(path);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        _buildRecordStopControl(),
        const SizedBox(width: 20),
        _buildText(),
        const SizedBox(width: 20),
      ],
    );
  }

  @override
  void dispose() {
    _timer?.cancel();
    _recordSub?.cancel();
    _amplitudeSub?.cancel();
    _audioRecorder.dispose();
    super.dispose();
  }

  Widget _buildRecordStopControl() {
    late Icon icon;

    if (_recordState != RecordState.stop) {
      icon = const Icon(Icons.stop_rounded, color: Colors.red);
    } else {
      icon = const Icon(Icons.mic_rounded);
    }

    return IconButton(
      icon: icon,
      onPressed: () {
        (_recordState != RecordState.stop) ? _stop() : _start();
      },
    );
  }

  Widget _buildText() {
    if (_recordState != RecordState.stop) {
      return _buildTimer();
    }

    return const SizedBox.shrink();
  }

  Widget _buildTimer() {
    final String seconds = _formatNumber(_recordDuration);

    return Text(
      seconds,
      style: const TextStyle(color: Colors.red),
    );
  }

  String _formatNumber(int number) {
    String numberStr = number.toString();
    if (number < 10) {
      numberStr = '0$numberStr';
    }

    return numberStr;
  }

  void _startTimer() {
    _timer?.cancel();

    _timer = Timer.periodic(const Duration(seconds: 1), (Timer t) {
      setState(() => _recordDuration++);
    });
  }
}
