import 'package:dacit/services/globals.dart';

class Relationship {
  final List<String> providesFor;
  final List<String> receivesFrom;
  final int pin;

  const Relationship({
    required this.providesFor,
    required this.receivesFrom,
    required this.pin,
  });

  factory Relationship.fromJson(Map<String, dynamic> json) {
    log.info(json);
    return Relationship(
      providesFor: List<String>.from(json['provides_for']),
      receivesFrom: List<String>.from(json['receives_from']),
      pin: json['pin'],
    );
  }
}

class RelationshipAnswer {
  final int pin;
  final String targetUser;

  RelationshipAnswer(
    this.targetUser,
    this.pin,
  );

  Map<dynamic, dynamic> toJson() => {
        'target_user': targetUser,
        'voice_share_pin': pin,
      };
}
