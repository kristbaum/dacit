import 'package:dacit/services/globals.dart';
import 'package:dacit/services/text_stimulus.dart';

class SpeakerDis {
  final int mode;
  final TextStimulus firstStimulus;
  final String firstAudio;
  final TextStimulus secondStimulus;
  final String secondAudio;
  final bool isSameSpeaker;
  final int speakerDisId;

  const SpeakerDis(
      {required this.mode,
      required this.firstStimulus,
      required this.firstAudio,
      required this.secondStimulus,
      required this.secondAudio,
      required this.isSameSpeaker,
      required this.speakerDisId});

  factory SpeakerDis.fromJson(Map<String, dynamic> json) {
    log.info(json);
    TextStimulus firstStimulus =
        TextStimulus.fromJson(json['first_stimulus']);
    TextStimulus secondStimulus =
        TextStimulus.fromJson(json['second_stimulus']);

    String firstAudioUri = json["first_audio"];
    if (!firstAudioUri.contains('wikimedia.org')) {
      firstAudioUri = "${baseDomain}api/download/$firstAudioUri";
    }

    String secondAudioUri = json["second_audio"];
    if (!secondAudioUri.contains('wikimedia.org')) {
      secondAudioUri = "${baseDomain}api/download/$secondAudioUri";
    }
    SpeakerDis sd = SpeakerDis(
        mode: json['mode'],
        firstStimulus: firstStimulus,
        firstAudio: firstAudioUri,
        secondStimulus: secondStimulus,
        secondAudio: secondAudioUri,
        isSameSpeaker: json["same_speaker"],
        speakerDisId: json["speaker_dis_id"]);
    return sd;
  }
}

class SpeakerDisAnswer {
  final int speakerDisId;
  final int firstAudioRepeated;
  final int secondAudioRepeated;
  final bool isAnswerEqual;

  SpeakerDisAnswer(this.speakerDisId, this.firstAudioRepeated,
      this.secondAudioRepeated, this.isAnswerEqual);

  Map<dynamic, dynamic> toJson() => {
        'speaker_dis_id': speakerDisId,
        'first_audio_repeated': firstAudioRepeated,
        'second_audio_repeated': secondAudioRepeated,
        'is_answer_equal': isAnswerEqual,
      };
}
