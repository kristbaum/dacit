import 'package:fl_chart/fl_chart.dart';

class Stats {
  final int numAudio;
  final int numMinpair;
  final int numMinpairWeek;
  final int numCorrectMinpair;
  final int numCorrectMinpairWeek;
  final int numWordtraining;
  final int numWordtrainingWeek;
  final int numCorrectWordtraining;
  final int numCorrectWordtrainingWeek;
  final int numSpeakerDis;
  final int numSpeakerDisWeek;
  final int numCorrectSpeakerDis;
  final int numCorrectSpeakerDisWeek;
  final int trainingToday;
  final List<FlSpot> trainingMinPairweek;
  final List<FlSpot> trainingWordTrainingweek;
  final List<FlSpot> trainingSpeakerDisweek;
  //final List<FlSpot> trainingDataHourFlSpot;

  const Stats({
    required this.numAudio,
    required this.numMinpair,
    required this.numMinpairWeek,
    required this.numCorrectMinpair,
    required this.numCorrectMinpairWeek,
    required this.numWordtraining,
    required this.numWordtrainingWeek,
    required this.numCorrectWordtraining,
    required this.numCorrectWordtrainingWeek,
    required this.numSpeakerDis,
    required this.numSpeakerDisWeek,
    required this.numCorrectSpeakerDis,
    required this.numCorrectSpeakerDisWeek,
    required this.trainingToday,
    required this.trainingMinPairweek,
    required this.trainingWordTrainingweek,
    required this.trainingSpeakerDisweek,
    //required this.trainingDataHourFlSpot
  });

  factory Stats.fromJson(Map<String, dynamic> json) {
    return Stats(
        numAudio: json['num_audio'],
        trainingToday: json['training_today'],
        numMinpair: json['num_minpair'],
        numMinpairWeek: json['num_minpair_week'],
        numCorrectMinpair: json['num_correct_minpair'],
        numCorrectMinpairWeek: json['num_correct_minpair_week'],
        numWordtraining: json['num_wordtraining'],
        numWordtrainingWeek: json['num_wordtraining_week'],
        numCorrectWordtraining: json['num_correct_wordtraining'],
        numCorrectWordtrainingWeek: json['num_correct_wordtraining_week'],
        numSpeakerDis: json['num_speakerdis'],
        numSpeakerDisWeek: json['num_speakerdis_week'],
        numCorrectSpeakerDis: json['num_correct_speakerdis'],
        numCorrectSpeakerDisWeek: json['num_correct_speakerdis_week'],
        trainingMinPairweek: json['training_min_pair_week']
            .entries
            .map((entry) {
              final x = int.parse(entry.key);
              final y = entry.value.toDouble();
              return FlSpot(x.toDouble(), y);
            })
            .toList()
            .cast<FlSpot>(),
        trainingWordTrainingweek: json['training_word_training_week']
            .entries
            .map((entry) {
              final x = int.parse(entry.key);
              final y = entry.value.toDouble();
              return FlSpot(x.toDouble(), y);
            })
            .toList()
            .cast<FlSpot>(),
        trainingSpeakerDisweek: json['training_speaker_dis_week']
            .entries
            .map((entry) {
              final x = int.parse(entry.key);
              final y = entry.value.toDouble();
              return FlSpot(x.toDouble(), y);
            })
            .toList()
            .cast<FlSpot>());
    // trainingDataHourFlSpot: json['training_data_hour']
    //     .entries
    //     .map((entry) {
    //       final x = int.parse(entry.key);
    //       final y = entry.value.toDouble();
    //       return FlSpot(x.toDouble(), y);
    //     })
    //     .toList()
    //     .cast<FlSpot>());
  }
}

class StatsHome {
  final int numAudio;
  final int trainingToday;

  const StatsHome({required this.numAudio, required this.trainingToday});

  factory StatsHome.fromJson(Map<String, dynamic> json) {
    return StatsHome(
      numAudio: json['num_audio'],
      trainingToday: json['training_today'],
    );
  }
}
