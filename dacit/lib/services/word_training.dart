import 'package:dacit/services/globals.dart';
import 'package:dacit/services/text_stimulus.dart';

class WordTraining {
  final int wordTrainingId;
  final TextStimulus selectedStimulus;
  final String selectedAudio;
  final List allAnswers;

  const WordTraining(
      {required this.wordTrainingId,
      required this.selectedStimulus,
      required this.selectedAudio,
      required this.allAnswers});

  factory WordTraining.fromJson(Map<String, dynamic> json) {
    TextStimulus selectedStimulus =
        TextStimulus.fromJson(json['selected_stimulus']);
    List allAnswers = [];
    allAnswers.add(selectedStimulus);
    for (final stimulus in json["additional_answers"]) {
      allAnswers.add(TextStimulus.fromJson(stimulus));
    }
    allAnswers.shuffle();

    String selectedAudioUri = json["selected_audio"];
    if (!selectedAudioUri.contains('wikimedia.org')) {
      selectedAudioUri = "${baseDomain}api/download/$selectedAudioUri";
    }
    log.info("Selected audio: $selectedAudioUri");
    WordTraining wt = WordTraining(
        wordTrainingId: json['word_training_id'],
        selectedStimulus: selectedStimulus,
        selectedAudio: selectedAudioUri,
        allAnswers: allAnswers);
        return wt;
  }
}

class WordTrainingAnswer {
  final int wordTrainingId;
  final int audioRepeated;
  final int userTsSelection;

  WordTrainingAnswer(
      this.wordTrainingId, this.audioRepeated, this.userTsSelection);

  Map<dynamic, dynamic> toJson() => {
        'word_training_id': wordTrainingId,
        'audio_repeated': audioRepeated,
        'user_ts_selection': userTsSelection,
      };
}
