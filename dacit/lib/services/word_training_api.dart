import 'dart:convert';
import 'package:dacit/services/globals.dart';
import 'package:dacit/services/word_training.dart';
import 'package:http/http.dart' as http;

Future<void> postWordTrainingAnswer(
    int index, WordTraining activeWordTraining, int audioRepeated) async {
  WordTrainingAnswer wordTrainingAnswer = WordTrainingAnswer(
      activeWordTraining.wordTrainingId,
      audioRepeated,
      activeWordTraining.allAnswers[index].id);
  log.info("trying to send result msg");
  log.info(json.encode(wordTrainingAnswer));
  final response = await http.post(Uri.parse('${baseDomain}api/trainword/'),
      headers: {
        "Authorization": "Token ${user.token}",
        "content-type": "application/json",
      },
      body: json.encode(wordTrainingAnswer));
  log.info("Successfully posted");
  if (response.statusCode == 200) {
    log.info("Send result successful");
  } else {
    throw Exception('Sending result not possible');
  }
}

Future<WordTraining> fetchWordTraining() async {
  final response = await http.get(
    Uri.parse('${baseDomain}api/trainword'),
    headers: {
      "Authorization": "Token ${user.token}",
    },
  );

  if (response.statusCode == 200) {
    log.info("Downloading new Wordtraing");
    log.info(jsonDecode(utf8.decode(response.bodyBytes)).toString());
    return WordTraining.fromJson(jsonDecode(utf8.decode(response.bodyBytes)));
  } else {
    throw Exception('Gerade sind keine neuen Inhalte verfügbar, prüfen Sie ihre Internetverbindung');
  }
}
