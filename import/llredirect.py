import csv
import requests

# Open the CSV file and read the contents
with open('LINGUA_LIBRE_DATA.csv', newline='') as csvfile, open('LINGUA_LIBRE_DATA_NEW.csv', 'a', newline='') as new_csvfile:
    reader = csv.DictReader(csvfile, delimiter=';')
    writer = csv.DictWriter(new_csvfile, fieldnames=reader.fieldnames, delimiter=';')
    writer.writeheader()

    # Skip the first 2604 rows
    for i, row in enumerate(reader):
        if i < 12057:
            continue
        print(f"Selected row: {i+1}")
        # Get the URL from the "file" column
        url = row['file']
        
        # Make a GET request to the URL and get the redirected URL
        response = requests.get(url, allow_redirects=True)
        redirected_url = response.url
        
        # Update the "file" column with the redirected URL
        row['file'] = redirected_url
        
        # Append the updated row to the list of rows
        writer.writerow(row)
        
